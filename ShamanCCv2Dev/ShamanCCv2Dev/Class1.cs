﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

using ShamanCCv2Dev.GUI;
using ShamanCCv2Dev.Constants;
using ShamanCCv2Dev.MoreMethods;
using ShamanCCv2Dev.Settings;

using ZzukBot.ExtensionFramework;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;
using ZzukBot.Settings;
using ZzukBot.Constants;

using Enums = ZzukBot.Constants.Enums;

namespace ShamanCCv2Dev
{
    [Export(typeof(CustomClass))]
    public class zShaman : CustomClass
    {
        public CCGui guiFormObject = new CCGui();
        public CCSettings settingsCC = new CCSettings();
        public zShamanSettingsGroupHeal settingsGroupHeal = new zShamanSettingsGroupHeal();
        public ShamanConstants shamanConstanctsObj = new ShamanConstants();
        public CCLogForm logForm = new CCLogForm();
        public WelcomeForm welcomeForm = new WelcomeForm();
        public HealForm groupHealForm = new HealForm();
        public WoWUnit lastHealedGroup = null;
        public List<WoWUnit> ignoredUnits = new List<WoWUnit>();
        public List<string> errorsThisSession = new List<string>();

        public LocalPlayer playerObj;

        /// <summary>
        /// //////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>

        public override string Author => "SeeSharp";
        public override Enums.ClassId Class => Enums.ClassId.Shaman;//This is in ZZukBot.Constants
        public override string Name => shamanConstanctsObj.ccName + " - " + shamanConstanctsObj.ccVersion + " Beta";

        public override bool Load()
        {//This should be called anytime the custom class loads, do things like set settings from a file here
            try
            {
                settingsCC = OptionManager.Get(shamanConstanctsObj.ccName).LoadFromJson<CCSettings>();
                settingsGroupHeal = OptionManager.Get(shamanConstanctsObj.ccName + "GroupHeal").LoadFromJson<zShamanSettingsGroupHeal>();
                Helpers.PrintToChat("CC Loaded");
                //settingsCC = OptionManager.Get(shamanConstanctsObj.ccName).LoadFromJson<CCSettings>();
            }
            catch(Exception exc)
            {
                //settingsCC = new CCSettings();
                //settingsCC.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                Helpers.PrintToChat("Error Loading settings " + exc.GetType().ToString());
                settingsCC.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                logForm.richTextBoxLog.Text += exc.ToString() + Environment.NewLine;
            }
            welcomeForm.parentCCObj = this;
            welcomeForm.labelVersionText.Text = shamanConstanctsObj.ccVersion;
            try
            {
                //check for complete object. If object is incomplete, create a new one
                if (settingsCC.useLightningShield)
                {
                    settingsCC.useLightningShield = settingsCC.useLightningShield;
                }
            }
            catch(Exception ex)
            {
                welcomeForm.Show();
                settingsCC = new CCSettings();
                settingsCC.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                OptionManager.Get(shamanConstanctsObj.ccName).SaveToJson(settingsCC);
                Helpers.PrintToChat("JSON File Created!");
            }
            try
            {
                //check for complete object. If object is incomplete, create a new one
                if (settingsGroupHeal.groupHealHealingWaveHPPercent != -1)
                {
                    settingsGroupHeal.groupHealHealingWaveHPPercent = settingsGroupHeal.groupHealHealingWaveHPPercent;
                }
            }
            catch (Exception ex)
            {
                settingsGroupHeal = new zShamanSettingsGroupHeal();
                //settingsCC.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                OptionManager.Get(shamanConstanctsObj.ccName + "GroupHeal").SaveToJson(settingsGroupHeal);
                Helpers.PrintToChat("JSON GroupHeal File Created!");
            }
            groupHealForm.parentCCObj = this;
            logForm.richTextBoxLog.Text += "[CC] -" + DateTime.Now.ToShortTimeString() + " - Loaded" + Environment.NewLine;
            return true;
        }

        public override bool OnBuff()
        {//returns true when done buffing, this is called during the onRest functions
            try
            {

                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm || ObjectManager.Instance.Player.ManaPercent < 18)
                {
                    return false;
                }

                if (Spell.Instance.IsShapeShifted)
                {
                    return false;
                }

                if (settingsCC.useLightningShield && Helpers.CanCast("Lightning Shield") && !ObjectManager.Instance.Player.GotAura("Lightning Shield")) //lightning shield
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return false;
                    }
                    Spell.Instance.Cast("Lightning Shield");
                    logForm.richTextBoxLog.Text += "[Buff] -" + DateTime.Now.ToShortTimeString() + " - Lightning Shield" + Environment.NewLine;
                    Helpers.PrintToChat("Buffing - Lightning Shield - " + Spell.Instance.GetSpellRank("Lightning Shield").ToString());
                    return false;
                }
                if(!ObjectManager.Instance.Player.IsMainhandEnchanted() && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault))) // weapon buff
                {
                    if (Spell.Instance.IsShapeShifted)
                    {
                        Spell.Instance.CancelShapeshift();
                        return false;
                    }
                    Spell.Instance.Cast(zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault));
                    logForm.richTextBoxLog.Text += "[Buff] -" + DateTime.Now.ToShortTimeString() + " - Weapon Enchant" + Environment.NewLine;
                    Helpers.PrintToChat("Buffing - " + zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault) + " - " + Spell.Instance.GetSpellRank(zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault)).ToString());
                    return false;
                }
            }
            catch(Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show("Welcome To zShaman!" + Environment.NewLine + "Version: " + shamanConstanctsObj.ccVersion + Environment.NewLine + Environment.NewLine + "(if this is not your first launch, report the bug!)");
                Helpers.PrintToChat("Buff Error");
                if (!errorsThisSession.Contains(ex.ToString()))
                {
                    ZzukBot.Helpers.Extensions.LogTo(ex.ToString() + Environment.NewLine, "zShamanError");
                    errorsThisSession.Add(ex.ToString());
                }
            }
            return true;
        }

        public override void OnPull()
        {//How we pull, make sure the target is not null
            try
            {
                WoWUnit tar = ObjectManager.Instance.Target;
                if (tar == null)
                {
                    return;
                }
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }
                if(tar.Reaction == Enums.UnitReaction.Friendly)
                {
                    return;
                }
                if (CustomClasses.Instance.Current.CombatDistance != settingsCC.combatPullRange)
                {
                    CustomClasses.Instance.Current.CombatDistance = settingsCC.combatPullRange;
                }
                if (!Helpers.CanCast(settingsCC.combatPullSpell) || settingsCC.combatPullSpell == "" || ObjectManager.Instance.Player.ManaPercent < 20)
                {
                    if (CustomClasses.Instance.Current.CombatDistance != 3)
                    {
                        CustomClasses.Instance.Current.CombatDistance = 3;
                    }
                }
                if (ObjectManager.Instance.Player.GotAura("Ghost Wolf"))
                {
                    Spell.Instance.CancelShapeshift();
                    return;
                }
                if (tar.DistanceToPlayer > settingsCC.combatPullRange)
                {
                    return;
                }
                else if (Helpers.CanCast(settingsCC.combatPullSpell))
                {
                    Spell.Instance.CastWait(settingsCC.combatPullSpell,250);
                    logForm.richTextBoxLog.Text += "[CC] -" + DateTime.Now.ToShortTimeString() + " - PULLING" + Environment.NewLine;
                    Helpers.PrintToChat("Pulling with " + settingsCC.combatPullSpell);
                    ZzukBot.Helpers.Wait.For("Pull", 500);
                    return;
                }
                Spell.Instance.Attack();
            }
            catch (Exception ex)
            {
                if (!errorsThisSession.Contains(ex.ToString()))
                {
                    ZzukBot.Helpers.Extensions.LogTo(ex.ToString() + Environment.NewLine, "zShamanError");
                    errorsThisSession.Add(ex.ToString());
                }
                logForm.richTextBoxLog.Text += ex.ToString() + Environment.NewLine;
            }

        }

        public override void OnFight()
        {//The combat routine AFTER pulling
            try
            {
                if (ObjectManager.Instance.Player.GotAura("Ghost Wolf"))
                {
                    Spell.Instance.CancelShapeshift();
                }
                //casting and other returns
                if (CustomClasses.Instance.Current.CombatDistance != 3)
                {
                    CustomClasses.Instance.Current.CombatDistance = 3;
                }
                if (settingsCC.useGroupHeal)
                {
                    if (GroupHealLoop())
                    {
                        return;
                    }
                    if (settingsCC.groupHealOnly)
                    {
                        return;
                    }
                }
                else
                {
                    if (ObjectManager.Instance.Player.CastingAsName == "Healing Wave" && ObjectManager.Instance.Player.HealthPercent > settingsCC.combatHealBelowPercentHP)
                    {
                        Spell.Instance.StopCasting();
                        Helpers.PrintToChat("Canceling double heal");
                        return;
                    }
                    if (settingsCC.combatUseHeals && ObjectManager.Instance.Player.HealthPercent <= settingsCC.combatHealBelowPercentHP && Helpers.CanCast("Healing Wave"))
                    {
                        Spell.Instance.Cast("Healing Wave");
                        return;
                    }
                }
                //auto attack
                if (settingsCC.combatUseAutoAttack)
                {
                    Spell.Instance.Attack(); //auto attack
                }
                else
                {
                    Spell.Instance.StopAttack();
                }
                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }
                if (ObjectManager.Instance.Player.ManaPercent <= settingsCC.combatKeepAboveManaPercent) //return if not enough mana
                {
                    logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Saving Mana" + Environment.NewLine;
                    return;
                }
                if (ObjectManager.Instance.Target.IsFleeing)
                {
                    if (Helpers.CanCast("Earth Shock"))
                    {
                        Spell.Instance.Cast("Earth Shock");
                        logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Flee Earth Shock" + Environment.NewLine;
                        return;
                    }
                    else if (Helpers.CanCast("Lightning Bolt"))
                    {
                        Spell.Instance.Cast("Lightning Bolt");
                        logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Flee Lightning Bolt" + Environment.NewLine;
                        return;
                    }
                }
                if(Spell.Instance.GetSpellRank("Earth Shock") == 0 && Helpers.CanCast("Lightning Bolt"))
                {
                    Spell.Instance.Cast("Lightning Bolt");
                    logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Lightning Bolt" + Environment.NewLine;
                    return;
  
                }
                //re-shield
                if (settingsCC.combatReApplyLightningShield && Helpers.CanCast("Lightning Shield") && !ObjectManager.Instance.Player.GotAura("Lightning Shield")) //lightning shield
                {
                    Spell.Instance.CastWait("Lightning Shield",4000);
                    logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Lightning Shield Re-apply" + Environment.NewLine;
                    return;
                }
                //re-enchant
                if (!ObjectManager.Instance.Player.IsMainhandEnchanted() && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault))) // weapon buff
                {
                    Spell.Instance.Cast(zShamanHelpers.ShamanEnumToSpellNameWeaponBuff(settingsCC.weaponBuffDefault));
                    logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Weapon Re-enchant" + Environment.NewLine;
                    return;
                }
                //filler shock
                if (Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameShockType(settingsCC.combatShockTypeDefault)) && settingsCC.combatUseShockSpells) //earth shock
                {
                    if (settingsCC.combatUseStormstrike && Helpers.CanCast("Stormstrike") && ObjectManager.Instance.Target.HealthPercent > 35 && ObjectManager.Instance.Player.ManaPercent > 40)
                    {
                        Spell.Instance.Cast("Stormstrike");
                        logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - STORMSTRIKE!" + Environment.NewLine;
                        return;
                    }
                    Spell.Instance.Cast(zShamanHelpers.ShamanEnumToSpellNameShockType(settingsCC.combatShockTypeDefault));
                    logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Filler Shock" + Environment.NewLine;
                    return;
                }
                //totems
                if (totemLoop())
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Helpers.PrintToChat("Combat Error. Logged.");
                if (!errorsThisSession.Contains(ex.ToString()))
                {
                    ZzukBot.Helpers.Extensions.LogTo(ex.ToString() + Environment.NewLine, "zShamanError");
                    errorsThisSession.Add(ex.ToString());
                }
                logForm.richTextBoxLog.Text += ex.ToString() + Environment.NewLine;
            }


        }

        public override void OnRest()
        {//What to do when told to rest (%mana and %health are low enough)
            try
            {
                if (ObjectManager.Instance.Player.CastingAsName == "Healing Wave" && ObjectManager.Instance.Player.HealthPercent > settingsCC.restHealHPPercent)
                {
                    Spell.Instance.StopCasting();
                    Helpers.PrintToChat("Canceling Heal!");
                    return;
                }

                if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
                {
                    return;
                }

                if (settingsCC.restHeal && ObjectManager.Instance.Player.HealthPercent <= settingsCC.restHealHPPercent && Helpers.CanCast("Healing Wave") && !ObjectManager.Instance.Player.IsDrinking && !ObjectManager.Instance.Player.IsEating)
                {
                    Spell.Instance.Cast("Healing Wave");
                    logForm.richTextBoxLog.Text += "[Rest] -" + DateTime.Now.ToShortTimeString() + " - Healing Wave" + Environment.NewLine;
                    return;
                }

                if (!ObjectManager.Instance.Player.IsDrinking && Inventory.Instance.GetItemCount(settingsCC.restDrink) > 0 && ObjectManager.Instance.Player.ManaPercent < 85)
                {
                    ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == settingsCC.restDrink).Use();
                    //Helpers.PrintToChat("Drinking " + settingsCC.restDrink);
                    logForm.richTextBoxLog.Text += "[Rest] -" + DateTime.Now.ToShortTimeString() + " - Drinking" + Environment.NewLine;
                    ZzukBot.Helpers.Wait.For("zShaman Drink", 750);
                    return;
                }
                if (!ObjectManager.Instance.Player.IsEating && Inventory.Instance.GetItemCount(settingsCC.restFood) > 0 && ObjectManager.Instance.Player.HealthPercent < 85)
                {
                    ObjectManager.Instance.Items.FirstOrDefault(i => i.Name == settingsCC.restFood).Use();
                    //Helpers.PrintToChat("Eating " + settingsCC.restFood);
                    logForm.richTextBoxLog.Text += "[Rest] -" + DateTime.Now.ToShortTimeString() + " - Eating" + Environment.NewLine;
                    ZzukBot.Helpers.Wait.For("zShaman Eat", 750);
                    return;
                }
            }
            catch (Exception ex)
            {
                Helpers.PrintToChat("ERROR RESTING");
                if (!errorsThisSession.Contains(ex.ToString()))
                {
                    ZzukBot.Helpers.Extensions.LogTo(ex.ToString() + Environment.NewLine, "zShamanError");
                    errorsThisSession.Add(ex.ToString());
                }
                logForm.richTextBoxLog.Text += ex.ToString() + Environment.NewLine;
            }
}

        public override void ShowGui()
        {//Used to show the settings GUI if present. 
            guiFormObject.Dispose();
            guiFormObject = new CCGui();
            guiFormObject.parentCCObj = this;
            guiFormObject.settingsGui = settingsCC;
            guiFormObject.SettingsLoad();
            guiFormObject.Show();
        }

        public override void Unload()
        {//Called when the CC is being unloaded
            try
            {
                Helpers.PrintToChat("Unloading!");
                OptionManager.Get(shamanConstanctsObj.ccName).SaveToJson(settingsCC);
            }
            catch (Exception exc)
            {
                if (!errorsThisSession.Contains(exc.ToString()))
                {
                    ZzukBot.Helpers.Extensions.LogTo(exc.ToString() + Environment.NewLine, "zShamanError");
                    errorsThisSession.Add(exc.ToString());
                }
                Helpers.PrintToChat("Unloading Error! " + exc.GetType().ToString());
            }
        }

        public override void _Dispose()
        {//Should be called when completely unloading the CC
            Helpers.PrintToChat("Disposing of CC memory!");
        }

        bool GroupHealLoop()
        {
            bool party1Exists = true;
            bool party2Exists = true;
            bool party3Exists = true;
            bool party4Exists = true;
            LocalPlayer pYou = ObjectManager.Instance.Player;
            WoWUnit p1 = ObjectManager.Instance.Party1;
            WoWUnit p2 = ObjectManager.Instance.Party2;
            WoWUnit p3 = ObjectManager.Instance.Party3;
            WoWUnit p4 = ObjectManager.Instance.Party4;
            if (p1 == null)
            {
                party1Exists = false;
            }
            if (p2 == null)
            {
                party2Exists = false;
            }
            if (p3 == null)
            {
                party3Exists = false;
            }
            if (p4 == null)
            {
                party4Exists = false;
            }

            if (pYou.CastingAsName == "Healing Wave")
            {
                if (lastHealedGroup != null && lastHealedGroup.HealthPercent > settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    Spell.Instance.StopCasting();
                    Helpers.PrintToChat("Canceling Double heal");
                    lastHealedGroup = null;
                }
                return true;
            }
            else
            {
                lastHealedGroup = null;
            }
            if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.IsDead || ObjectManager.Instance.Player.InGhostForm)
            {
                return true;
            }

            if (pYou.CastingAsName == " Lesser Healing Wave")
            {

                return true;
            }

            if (pYou.ManaPercent >= settingsGroupHeal.ggroupHealHealingWaveManaMin)
            {
                if (pYou.HealthPercent <= settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    //pYou.SetTarget();
                    Spell.Instance.Cast("Healing Wave");
                    lastHealedGroup = pYou;
                    return true;
                }
                if (party1Exists && p1.HealthPercent <= settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    pYou.SetTarget(p1);
                    Spell.Instance.Cast("Healing Wave");
                    lastHealedGroup = p1;
                    return true;
                }
                if (party2Exists && p2.HealthPercent <= settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    pYou.SetTarget(p2);
                    Spell.Instance.Cast("Healing Wave");
                    lastHealedGroup = p2;
                    return true;
                }
                if (party3Exists && p3.HealthPercent <= settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    pYou.SetTarget(p3);
                    Spell.Instance.Cast("Healing Wave");
                    lastHealedGroup = p3;
                    return true;
                }
                if (party4Exists && p4.HealthPercent <= settingsGroupHeal.groupHealHealingWaveHPPercent)
                {
                    pYou.SetTarget(p4);
                    Spell.Instance.Cast("Healing Wave");
                    lastHealedGroup = p4;
                    return true;
                }
            }

            if (pYou.ManaPercent >= settingsGroupHeal.ggroupHealHealingWaveManaMin && settingsGroupHeal.groupHealHealingWaveDownskill && Spell.Instance.GetSpellRank("Healing Wave") > 1)
            {
                if (party1Exists && p1.HealthPercent <= 85)
                {
                    pYou.SetTarget(p1);
                    Spell.Instance.Cast("Healing Wave", Spell.Instance.GetSpellRank("Healing Wave") - 1);
                    return true;
                }
                if (party2Exists && p2.HealthPercent <= 85)
                {
                    pYou.SetTarget(p2);
                    Spell.Instance.Cast("Healing Wave", Spell.Instance.GetSpellRank("Healing Wave") - 1);
                    return true;
                }
                if (party3Exists && p3.HealthPercent <= 85)
                {
                    pYou.SetTarget(p3);
                    Spell.Instance.Cast("Healing Wave", Spell.Instance.GetSpellRank("Healing Wave") - 1);
                    return true;
                }
                if (party4Exists && p4.HealthPercent <= 85)
                {
                    pYou.SetTarget(p4);
                    Spell.Instance.Cast("Healing Wave", Spell.Instance.GetSpellRank("Healing Wave") - 1);
                    return true;
                }
            }
            return false;
        }

        bool totemLoop()
        {
            if (settingsCC.totemEnabledEarth && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameEarthTotem(settingsCC.totemDefaultEarth)) && zShamanHelpers.ShouldPlaceTotem(zShamanHelpers.ShamanEnumToSpellNameEarthTotem(settingsCC.totemDefaultEarth), (int)settingsCC.maxTotemDistance)) //earth
            {
                Spell.Instance.CastWait(zShamanHelpers.ShamanEnumToSpellNameEarthTotem(settingsCC.totemDefaultEarth), 1000);
                logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Earth Totem" + Environment.NewLine;
                return true;
            }
            if (settingsCC.totemEnabledFire && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameFireTotem(settingsCC.totemDefaultFire)) && zShamanHelpers.ShouldPlaceTotem(zShamanHelpers.ShamanEnumToSpellNameFireTotem(settingsCC.totemDefaultFire), (int)settingsCC.maxTotemDistance)) //fire
            {
                Spell.Instance.CastWait(zShamanHelpers.ShamanEnumToSpellNameFireTotem(settingsCC.totemDefaultFire), 1000);
                logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Fire Totem" + Environment.NewLine;
                return true;
            }
            if (settingsCC.totemEnabledWater && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameWaterTotem(settingsCC.totemDefaultWater)) && zShamanHelpers.ShouldPlaceTotem(zShamanHelpers.ShamanEnumToSpellNameWaterTotem(settingsCC.totemDefaultWater), (int)settingsCC.maxTotemDistance)) //water
            {
                Spell.Instance.CastWait(zShamanHelpers.ShamanEnumToSpellNameWaterTotem(settingsCC.totemDefaultWater), 1000);
                logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Water Totem" + Environment.NewLine;
                return true;
            }
            if (settingsCC.totemEnabledAir && Helpers.CanCast(zShamanHelpers.ShamanEnumToSpellNameAirTotem(settingsCC.totemDefaultAir)) && zShamanHelpers.ShouldPlaceTotem(zShamanHelpers.ShamanEnumToSpellNameAirTotem(settingsCC.totemDefaultAir), (int)settingsCC.maxTotemDistance)) //air
            {
                Spell.Instance.CastWait(zShamanHelpers.ShamanEnumToSpellNameAirTotem(settingsCC.totemDefaultAir), 1000);
                logForm.richTextBoxLog.Text += "[Combat] -" + DateTime.Now.ToShortTimeString() + " - Air Totem" + Environment.NewLine;
                return true;
            }
            return true;
        }


    }
}
