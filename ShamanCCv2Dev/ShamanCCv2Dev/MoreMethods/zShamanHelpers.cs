﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ZzukBot.Game.Statics;
using ZzukBot.Objects;

using ShamanCCv2Dev.Constants;

namespace ShamanCCv2Dev.MoreMethods
{
    class zShamanHelpers
    {
        public static bool PlayerHasBuff(string buffToCheckFor)
        {
            List<int> aurasListTemp = ObjectManager.Instance.Player.Auras;
            return true;
        }

        public static void PrintAurasToGame()
        {
            List<int> aurasListTemp = ObjectManager.Instance.Player.Auras;
            Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('zShaman: Aura List Start-----')");
            for (int i = 0; i < aurasListTemp.Count; i+=1) // Loop with for.
            {
                Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('zShaman: " + aurasListTemp[i].ToString() + "')");
            }
            Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('zShaman: Aura List End-----')");
        }

        public static bool ShouldPlaceTotem(string totemNameToCheckFor, int maxRange)
        {
            if (totemNameToCheckFor == "Stoneskin Totem")
            {
                if (ObjectManager.Instance.Player.GotAura("Stoneskin"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (ObjectManager.Instance.Player.IsTotemSpawned(totemNameToCheckFor) == -1 || (ObjectManager.Instance.Player.IsTotemSpawned(totemNameToCheckFor) > maxRange))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static string ShamanEnumToSpellNameEarthTotem(ShamanEnums.EarthTotems earthTotemToUse)
        {
            string tempString = "";
            switch(earthTotemToUse)
            {
                case ShamanEnums.EarthTotems.Stoneskin:
                    {
                        tempString = "Stoneskin Totem";
                        break;
                    }
                case ShamanEnums.EarthTotems.Earthbind:
                    {
                        tempString = "Earthbind Totem";
                        break;
                    }
                case ShamanEnums.EarthTotems.Stoneclaw:
                    {
                        tempString = "Stoneclaw Totem";
                        break;
                    }
                case ShamanEnums.EarthTotems.StregnthOfEarth:
                    {
                        tempString = "Stregnth of Earth Totem";
                        break;
                    }
            }
            return tempString;
        }
        public static string ShamanEnumToSpellNameFireTotem(ShamanEnums.FireTotems totemToUse)
        {
            string tempString = "";
            switch (totemToUse)
            {
                case ShamanEnums.FireTotems.FireNova:
                    {
                        tempString = "Fire Nova Totem";
                        break;
                    }
                case ShamanEnums.FireTotems.FlameTongue:
                    {
                        tempString = "Flametongue Totem";
                        break;
                    }
                case ShamanEnums.FireTotems.FrostResistance:
                    {
                        tempString = "Frost Resistance Totem";
                        break;
                    }
                case ShamanEnums.FireTotems.Magma:
                    {
                        tempString = "Magma Totem";
                        break;
                    }
                case ShamanEnums.FireTotems.Searing:
                    {
                        tempString = "Searing Totem";
                        break;
                    }
            }
            return tempString;
        }
        public static string ShamanEnumToSpellNameWaterTotem(ShamanEnums.WaterTotems totemToUse)
        {
            string tempString = "";
            switch (totemToUse)
            {
                case ShamanEnums.WaterTotems.DiseaseCleansing:
                    {
                        tempString = "Disease Cleansing Totem";
                        break;
                    }
                case ShamanEnums.WaterTotems.PoisonCleansing:
                    {
                        tempString = "Poison Cleansing Totem";
                        break;
                    }
                case ShamanEnums.WaterTotems.FireResistance:
                    {
                        tempString = "Fire Resistance Totem";
                        break;
                    }
                case ShamanEnums.WaterTotems.HealingStream:
                    {
                        tempString = "Healing Stream Totem";
                        break;
                    }
                case ShamanEnums.WaterTotems.ManaSpring:
                    {
                        tempString = "Mana Spring Totem";
                        break;
                    }
                case ShamanEnums.WaterTotems.ManaTide:
                    {
                        tempString = "Mana Tide Totem";
                        break;
                    }
            }
            return tempString;
        }
        public static string ShamanEnumToSpellNameAirTotem(ShamanEnums.AirTotems totemToUse)
        {
            string tempString = "";
            switch (totemToUse)
            {
                case ShamanEnums.AirTotems.Grounding:
                    {
                        tempString = "Grounding Totem";
                        break;
                    }
                case ShamanEnums.AirTotems.NatureResistance:
                    {
                        tempString = "Nature Resistance Totem";
                        break;
                    }
                case ShamanEnums.AirTotems.Sentry:
                    {
                        tempString = "Sentry Totem";
                        break;
                    }
                case ShamanEnums.AirTotems.Windfurry:
                    {
                        tempString = "Windfury Totem";
                        break;
                    }
            }
            return tempString;
        }
        public static string ShamanEnumToSpellNameWeaponBuff(ShamanEnums.WeaponBuffs buffToUse)
        {
            string tempString = "";
            switch (buffToUse)
            {
                case ShamanEnums.WeaponBuffs.Rockbiter:
                    {
                        tempString = "Rockbiter Weapon";
                        break;
                    }
                case ShamanEnums.WeaponBuffs.Flametongue:
                    {
                        tempString = "Flametongue Weapon";
                        break;
                    }
                case ShamanEnums.WeaponBuffs.Frostbrand:
                    {
                        tempString = "Frostbrand Weapon";
                        break;
                    }
                case ShamanEnums.WeaponBuffs.Windfury:
                    {
                        tempString = "Windfury Weapon";
                        break;
                    }
            }
            return tempString;
        }
        public static string ShamanEnumToSpellNameShockType(ShamanEnums.ShockTypes shockTypeToUse)
        {
            string tempString = "";
            switch (shockTypeToUse)
            {
                case ShamanEnums.ShockTypes.Earth:
                    {
                        tempString = "Earth Shock";
                        break;
                    }
                case ShamanEnums.ShockTypes.Flame:
                    {
                        tempString = "Flame Shock";
                        break;
                    }
                case ShamanEnums.ShockTypes.Frost:
                    {
                        tempString = "Frost Shock";
                        break;
                    }
            }
            return tempString;
        }

        public static int ItemCountFromName(string itemName)
        {
            WoWItem itemToCheckFor;
            itemToCheckFor = Inventory.Instance.GetItem(itemName);

            return Inventory.Instance.GetItemCount(itemToCheckFor.Id);
        }

    }
}
