﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShamanCCv2Dev.Constants
{
    public class ShamanEnums
    {
        public enum FireTotems
        {
            Searing,
            FireNova,
            Magma,
            FlameTongue,
            FrostResistance
        }

        public enum EarthTotems
        {
            Stoneskin,
            Earthbind,
            Stoneclaw,
            StregnthOfEarth
        }

        public enum WaterTotems
        {
            ManaSpring,
            FireResistance,
            HealingStream,
            DiseaseCleansing,
            PoisonCleansing,
            ManaTide
        }

        public enum AirTotems
        {
            Grounding,
            NatureResistance,
            Windfurry,
            Sentry
        }

        public enum WeaponBuffs
        {
            Rockbiter,
            Flametongue,
            Frostbrand,
            Windfury,
        }

        public enum ShockTypes
        {
            Earth,
            Flame,
            Frost
        }

        public enum SettingsPresets
        {
            Default,
            Lowbie,
            Experimental,
            Raider,
            PVP
        }

        public enum GroupTypes
        {
            Tank,
            Heal,
            DPS,
            Undefined
        }
    }
}
