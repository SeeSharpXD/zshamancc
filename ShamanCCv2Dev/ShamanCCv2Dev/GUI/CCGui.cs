﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

using ShamanCCv2Dev;
using ShamanCCv2Dev.Constants;
using ShamanCCv2Dev.Settings;
using ShamanCCv2Dev.MoreMethods;

using ZzukBot.Game.Statics;
using ZzukBot.Objects;
using ZzukBot.Settings;

namespace ShamanCCv2Dev.GUI
{
    public partial class CCGui : Form
    {
        public CCSettings settingsGui;
        public zShaman parentCCObj;
        public ShamanConstants shamanConstanctsObj = new ShamanConstants();

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public DevPanel devPanelObj = new DevPanel();

        public CCGui()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            SettingsSave();
            Helpers.PrintToChat("Settings Applied to Current Session");
            try
            {
                OptionManager.Get(shamanConstanctsObj.ccName).SaveToJson(settingsGui);
                Helpers.PrintToChat("Settings Saved to JSON");
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error Saving to JSON! " + Environment.NewLine + exc.GetType().ToString());
            }
            devPanelObj.Close();
            devPanelObj.Dispose();
            Helpers.PrintToChat("GUI Closed");
            this.Close();
        }

        private void CCGui_Load(object sender, EventArgs e)
        {
            comboBoxCCLanguage.SelectedIndex = 0;
            comboBoxCCLoadSettings.SelectedIndex = 0;
            labelCCName.Text = shamanConstanctsObj.guiPanelTitle + shamanConstanctsObj.ccVersion + " - Beta";
            labelAboutVersion.Text = "V " + shamanConstanctsObj.ccVersion + " - Enhancement";
        }

        private void pictureBoxExitGUI_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                devPanelObj.Close();
            }
            catch(Exception exc)
            {
                MessageBox.Show("Error closing devpanel");
            }
            devPanelObj.Dispose();
            this.Close();
            Helpers.PrintToChat("GUI Closed and Changes Discarded");
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
        public void SettingsLoad()
        {
            try
            {
                //load totem settings
                switch (settingsGui.totemDefaultEarth) //earth totem load
                {
                    /*
                        Stoneskin
                        Earthbind
                        Stoneclaw
                        Stregnth of Earth 
                     */
                    case ShamanEnums.EarthTotems.Stoneskin:
                        {
                            comboBoxEarthTotem.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.EarthTotems.Earthbind:
                        {
                            comboBoxEarthTotem.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.EarthTotems.Stoneclaw:
                        {
                            comboBoxEarthTotem.SelectedIndex = 2;
                            break;
                        }
                    case ShamanEnums.EarthTotems.StregnthOfEarth:
                        {
                            comboBoxEarthTotem.SelectedIndex = 3;
                            break;
                        }
                        
                }
                switch (settingsGui.totemDefaultFire) //Fire totem load
                {
                    /*
                        Searing
                        Fire Nova
                        Magma
                        Flametongue
                        Frost Resistance
                     */
                    case ShamanEnums.FireTotems.Searing:
                        {
                            comboBoxFireTotem.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.FireTotems.FireNova:
                        {
                            comboBoxFireTotem.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.FireTotems.Magma:
                        {
                            comboBoxFireTotem.SelectedIndex = 2;
                            break;
                        }
                    case ShamanEnums.FireTotems.FlameTongue:
                        {
                            comboBoxFireTotem.SelectedIndex = 3;
                            break;
                        }
                    case ShamanEnums.FireTotems.FrostResistance:
                        {
                            comboBoxFireTotem.SelectedIndex = 4;
                            break;
                        }
                }
                switch (settingsGui.totemDefaultWater) //Water totem load
                {
                    /*
                        Mana Spring
                        Fire Resistance
                        Healing Stream
                        Disease Cleansing
                        Mana Tide
                     */
                    case ShamanEnums.WaterTotems.ManaSpring:
                        {
                            comboBoxWaterTotem.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.WaterTotems.FireResistance:
                        {
                            comboBoxWaterTotem.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.WaterTotems.HealingStream:
                        {
                            comboBoxWaterTotem.SelectedIndex = 2;
                            break;
                        }
                    case ShamanEnums.WaterTotems.DiseaseCleansing:
                        {
                            comboBoxWaterTotem.SelectedIndex = 3;
                            break;
                        }
                    case ShamanEnums.WaterTotems.ManaTide:
                        {
                            comboBoxWaterTotem.SelectedIndex = 4;
                            break;
                        }
                    case ShamanEnums.WaterTotems.PoisonCleansing:
                        {
                            comboBoxWaterTotem.SelectedIndex = 5;
                            break;
                        }
                }
                switch (settingsGui.totemDefaultAir) //Air totem load
                {
                    /*
                        Grounding
                        Nature Resistance
                        Windfurry
                        Sentry
                     */
                    case ShamanEnums.AirTotems.Grounding:
                        {
                            comboBoxAirTotem.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.AirTotems.NatureResistance:
                        {
                            comboBoxAirTotem.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.AirTotems.Windfurry:
                        {
                            comboBoxAirTotem.SelectedIndex = 2;
                            break;
                        }
                    case ShamanEnums.AirTotems.Sentry:
                        {
                            comboBoxAirTotem.SelectedIndex = 3;
                            break;
                        }
                }

                //load totem enables
                checkBoxUseEarthTotem.Checked = settingsGui.totemEnabledEarth; //earth
                checkBoxUseFireTotem.Checked = settingsGui.totemEnabledFire; //fire
                checkBoxUseWaterTotem.Checked = settingsGui.totemEnabledWater; //water
                checkBoxUseAirTotem.Checked = settingsGui.totemEnabledAir; //air

                //load other totem settings
                numericUpDownTotemRange.Value = (decimal)settingsGui.maxTotemDistance; //max distance
                checkBoxSmartTotems.Checked = settingsGui.smartTotem; // smart totem

                //load buff settings
                checkBoxUseWeaponBuff.Checked = settingsGui.weaponUseBuff; //use buff

                switch (settingsGui.weaponBuffDefault) //weapon buff to use
                {
                    /*
                        Rockbiter
                        Flametongue
                        Frostbrand
                        Windfury
                     */
                    case ShamanEnums.WeaponBuffs.Rockbiter:
                        {
                            comboBoxWeaponBuff.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.WeaponBuffs.Flametongue:
                        {
                            comboBoxWeaponBuff.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.WeaponBuffs.Frostbrand:
                        {
                            comboBoxWeaponBuff.SelectedIndex = 2;
                            break;
                        }
                    case ShamanEnums.WeaponBuffs.Windfury:
                        {
                            comboBoxWeaponBuff.SelectedIndex = 3;
                            break;
                        }
                }
                checkBoxBuffWaterWalking.Checked = settingsGui.useWaterWalking; //water walking
                checkBoxBuffLightningShield.Checked = settingsGui.useLightningShield;

                //load rest settings
                textBoxRestFoodname.Text = settingsGui.restFood; //food to use
                textBoxRestDrinkname.Text = settingsGui.restDrink; //drink to use
                checkBoxRestUseHeal.Checked = settingsGui.restHeal; //heal on rest
                numericUpDownRestHealPercent.Value = (decimal)settingsGui.restHealHPPercent; //heal on rest HP percent

                //pull settings
                numericUpDownPullRange.Value = (decimal)settingsGui.combatPullRange; //pull range
                textBoxPullingSpell.Text = settingsGui.combatPullSpell; //pull spell

                checkBoxNoPullIfTargetSkull.Checked = settingsGui.combatNoPullOnTargetSkull;
                numericUpDownNoPullIfTargerLevelGreater.Value = (decimal)settingsGui.combatNoPullOnTargetLevelGreaterBy;
                numericUpDownNoPullIfTargetLevelLess.Value = (decimal)settingsGui.combatNoPullOnTargetLevelLessBy;

                //combat settings
                switch (settingsGui.combatShockTypeDefault) //shock type in combat default
                {
                    case ShamanEnums.ShockTypes.Earth:
                        {
                            comboBoxCombatPreferedShock.SelectedIndex = 0;
                            break;
                        }
                    case ShamanEnums.ShockTypes.Flame:
                        {
                            comboBoxCombatPreferedShock.SelectedIndex = 1;
                            break;
                        }
                    case ShamanEnums.ShockTypes.Frost:
                        {
                            comboBoxCombatPreferedShock.SelectedIndex = 2;
                            break;
                        }

                }
                checkBoxCombatUseShockSpells.Checked = settingsGui.combatUseShockSpells; //use shocks in combat
                checkBoxCombatAutoAttack.Checked = settingsGui.combatUseAutoAttack; //auto attack in combat
                checkBoxCombatUseHeal.Checked = settingsGui.combatUseHeals; //heal yourself in combat
                checkBoxCombatUseLesserHeal.Checked = settingsGui.combatUseLesserHeal;
                numericUpDownCombatHealHpPercent.Value = (decimal)settingsGui.combatHealBelowPercentHP; //heal in combat HP percent
                numericUpDownCombatLesserHealPercentHP.Value = (decimal)settingsGui.combatLesserHealBelowPercentHP;
                checkBoxCombatApplyFlameshock.Checked = settingsGui.combatUseFlameshockDot; //use flameshock DoT in combat
                checkBoxCombatReapplyLightningShield.Checked = settingsGui.combatReApplyLightningShield; //lightning shield
                checkBoxCombatUseStormstrike.Checked = settingsGui.combatUseStormstrike;
                numericUpDownKeepAboveManaPercentCombat.Value = (decimal)settingsGui.combatKeepAboveManaPercent;
                checkBoxHamanizerErrors.Checked = settingsGui.humanizeMakeErrors; //make errors

                checkBoxUseGroupHeals.Checked = settingsGui.useGroupHeal;
                checkBoxGroupHealOnly.Checked = settingsGui.groupHealOnly;

                Helpers.PrintToChat("GUI Loaded Successfully");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Loading Settings");
                parentCCObj.logForm.richTextBoxLog.Text += e.ToString() + Environment.NewLine;
                parentCCObj.logForm.Show();
            }

        }
        public void SettingsSave()
        {
            try
            {
                //save totem settings
                switch (comboBoxEarthTotem.SelectedIndex) //earth totem
                {
                    case 0:
                        {
                            settingsGui.totemDefaultEarth = ShamanEnums.EarthTotems.Stoneskin;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.totemDefaultEarth = ShamanEnums.EarthTotems.Earthbind;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.totemDefaultEarth = ShamanEnums.EarthTotems.Stoneclaw;
                            break;
                        }
                    case 3:
                        {
                            settingsGui.totemDefaultEarth = ShamanEnums.EarthTotems.StregnthOfEarth;
                            break;
                        }
                }

                switch (comboBoxFireTotem.SelectedIndex) //Fire totem
                {
                    /*
                        Searing
                        Fire Nova
                        Magma
                        Flametongue
                        Frost Resistance
                    */
                    case 0:
                        {
                            settingsGui.totemDefaultFire = ShamanEnums.FireTotems.Searing;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.totemDefaultFire = ShamanEnums.FireTotems.FireNova;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.totemDefaultFire = ShamanEnums.FireTotems.Magma;
                            break;
                        }
                    case 3:
                        {
                            settingsGui.totemDefaultFire = ShamanEnums.FireTotems.FlameTongue;
                            break;
                        }
                    case 4:
                        {
                            settingsGui.totemDefaultFire = ShamanEnums.FireTotems.FrostResistance;
                            break;
                        }
                }

                switch (comboBoxWaterTotem.SelectedIndex) //water totem
                {
                    /*
                        Mana Spring
                        Fire Resistance
                        Healing Stream
                        Disease Cleansing
                        Mana Tide
                    */
                    case 0:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.ManaSpring;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.FireResistance;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.HealingStream;
                            break;
                        }
                    case 3:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.DiseaseCleansing;
                            break;
                        }
                    case 4:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.ManaTide;
                            break;
                        }
                    case 5:
                        {
                            settingsGui.totemDefaultWater = ShamanEnums.WaterTotems.PoisonCleansing;
                            break;
                        }
                }

                switch (comboBoxAirTotem.SelectedIndex) //Air totem
                {
                    /*
                        Grounding
                        Nature Resistance
                        Windfurry
                        Sentry
                     */
                    case 0:
                        {
                            settingsGui.totemDefaultAir = ShamanEnums.AirTotems.Grounding;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.totemDefaultAir = ShamanEnums.AirTotems.NatureResistance;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.totemDefaultAir = ShamanEnums.AirTotems.Windfurry;
                            break;
                        }
                    case 3:
                        {
                            settingsGui.totemDefaultAir = ShamanEnums.AirTotems.Sentry;
                            break;
                        }
                }

                //save totem enables
                settingsGui.totemEnabledEarth = checkBoxUseEarthTotem.Checked; //earth
                settingsGui.totemEnabledFire = checkBoxUseFireTotem.Checked; //fire
                settingsGui.totemEnabledWater = checkBoxUseWaterTotem.Checked; //water
                settingsGui.totemEnabledAir = checkBoxUseAirTotem.Checked; //air

                //save other totem settings
                settingsGui.maxTotemDistance = (float)numericUpDownTotemRange.Value; //max distance
                settingsGui.smartTotem = checkBoxSmartTotems.Checked; //smart toems
                settingsGui.humanizeMakeErrors = checkBoxHamanizerErrors.Checked; //make errors

                //save buff settings
                settingsGui.weaponUseBuff = checkBoxUseWeaponBuff.Checked;
                switch (comboBoxWeaponBuff.SelectedIndex)
                {
                    /*
                        Rockbiter
                        Flametongue
                        Frostbrand
                        Windfury
                     */
                    case 0:
                        {
                            settingsGui.weaponBuffDefault = ShamanEnums.WeaponBuffs.Rockbiter;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.weaponBuffDefault = ShamanEnums.WeaponBuffs.Flametongue;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.weaponBuffDefault = ShamanEnums.WeaponBuffs.Frostbrand;
                            break;
                        }
                    case 3:
                        {
                            settingsGui.weaponBuffDefault = ShamanEnums.WeaponBuffs.Windfury;
                            break;
                        }
                }
                settingsGui.useWaterWalking = checkBoxBuffWaterWalking.Checked;
                settingsGui.useLightningShield = checkBoxBuffLightningShield.Checked;

                //save rest settings
                settingsGui.restFood = textBoxRestFoodname.Text; //rest food
                settingsGui.restDrink = textBoxRestDrinkname.Text; //rest drink
                settingsGui.restHeal = checkBoxRestUseHeal.Checked; //rest use heal spell
                settingsGui.restHealHPPercent = (float)numericUpDownRestHealPercent.Value; //rest heal spell mana

                //save pull settings
                settingsGui.combatPullRange = (float)numericUpDownPullRange.Value; //pull range
                settingsGui.combatPullSpell = textBoxPullingSpell.Text; ; //pull spell

                settingsGui.combatNoPullOnTargetSkull = checkBoxNoPullIfTargetSkull.Checked;
                settingsGui.combatNoPullOnTargetLevelGreaterBy = (float)numericUpDownNoPullIfTargerLevelGreater.Value;
                settingsGui.combatNoPullOnTargetLevelLessBy = (float)numericUpDownNoPullIfTargetLevelLess.Value;

                //save combat settings
                switch (comboBoxCombatPreferedShock.SelectedIndex) //shock type in combat default
                {
                    case 0:
                        {
                            settingsGui.combatShockTypeDefault = ShamanEnums.ShockTypes.Earth;
                            break;
                        }
                    case 1:
                        {
                            settingsGui.combatShockTypeDefault = ShamanEnums.ShockTypes.Flame;
                            break;
                        }
                    case 2:
                        {
                            settingsGui.combatShockTypeDefault = ShamanEnums.ShockTypes.Frost;
                            break;
                        }

                }
                settingsGui.combatUseShockSpells = checkBoxCombatUseShockSpells.Checked; //use shocks
                settingsGui.combatUseAutoAttack = checkBoxCombatAutoAttack.Checked; //auto attack
                settingsGui.combatUseHeals = checkBoxCombatUseHeal.Checked; //heal in combat
                settingsGui.combatUseLesserHeal = checkBoxCombatUseLesserHeal.Checked;
                settingsGui.combatHealBelowPercentHP = (float)numericUpDownCombatHealHpPercent.Value; //heal in combat below HP%
                settingsGui.combatLesserHealBelowPercentHP = (float)numericUpDownCombatLesserHealPercentHP.Value;
                settingsGui.combatUseFlameshockDot = checkBoxCombatApplyFlameshock.Checked; //use flameshock DoT in combat
                settingsGui.combatReApplyLightningShield = checkBoxCombatReapplyLightningShield.Checked; //lightning shield
                settingsGui.combatUseStormstrike = checkBoxCombatUseStormstrike.Checked;
                settingsGui.combatKeepAboveManaPercent = (float)numericUpDownKeepAboveManaPercentCombat.Value;

                settingsGui.useGroupHeal = checkBoxUseGroupHeals.Checked;
                settingsGui.groupHealOnly = checkBoxGroupHealOnly.Checked;

                Helpers.PrintToChat("GUI Settings Saved Successfully");
            }
            catch(Exception e)
            {
                parentCCObj.logForm.richTextBoxLog.Text += e.ToString() + Environment.NewLine;
                parentCCObj.logForm.Show();
            }

        }

        private void linkLabelSeeSharp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=1675");
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void buttonLoadCCDefaultConfigs_Click(object sender, EventArgs e)
        {
            switch(comboBoxCCLoadSettings.SelectedIndex)
            {
                case 0:
                    {
                        settingsGui.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Default);
                        break;
                    }
                case 1:
                    {
                        settingsGui.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Lowbie);
                        break;
                    }
                case 2:
                    {
                        settingsGui.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Experimental);
                        break;
                    }
                case 3:
                    {
                        settingsGui.SetDefaultConfigValues(ShamanEnums.SettingsPresets.Raider);
                        break;
                    }
                case 4:
                    {
                        settingsGui.SetDefaultConfigValues(ShamanEnums.SettingsPresets.PVP);
                        break;
                    }
            }
            SettingsLoad();
        }

        private void buttonReportBugFormShow_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=61&t=1101");
        }

        private void buttonDevPanelShow_Click(object sender, EventArgs e)
        {
            devPanelObj.Dispose();
            devPanelObj = new DevPanel();
            devPanelObj.Show();
        }

        private void pictureBoxExitGUI_Click(object sender, EventArgs e)
        {

        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkLabelZzuk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=58");
        }

        private void linkLabelEmu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/memberlist.php?mode=viewprofile&u=2");
        }

        private void buttonCCSaveSettingsToFile_Click(object sender, EventArgs e)
        {
          
        }

        private void buttonLoadSettingsFromFile_Click(object sender, EventArgs e)
        {

        }

        private void buttonAboutForumThread_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://zzukbot.com/forum/viewtopic.php?f=61&t=1101");
        }

        private void pictureBoxMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void saveFileDialogCurrentSettings_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void buttonShowCCLog_Click(object sender, EventArgs e)
        {
            parentCCObj.logForm.Show();
        }

        private void buttonWelcomeMessageShow_Click(object sender, EventArgs e)
        {
            parentCCObj.welcomeForm.Show();
        }

        private void buttonGroupHealSettings_Click(object sender, EventArgs e)
        {
            parentCCObj.groupHealForm.PreLoad();
            parentCCObj.groupHealForm.Show();
        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }
    }
}
