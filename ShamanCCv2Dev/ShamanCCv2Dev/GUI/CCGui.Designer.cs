﻿namespace ShamanCCv2Dev.GUI
{
    partial class CCGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CCGui));
            this.buttonClose = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.pictureBoxMinimize = new System.Windows.Forms.PictureBox();
            this.labelCCName = new System.Windows.Forms.Label();
            this.pictureBoxExitGUI = new System.Windows.Forms.PictureBox();
            this.comboBoxEarthTotem = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.checkBoxUseEarthTotem = new System.Windows.Forms.CheckBox();
            this.checkBoxUseAirTotem = new System.Windows.Forms.CheckBox();
            this.checkBoxUseWaterTotem = new System.Windows.Forms.CheckBox();
            this.checkBoxUseFireTotem = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownTotemRange = new System.Windows.Forms.NumericUpDown();
            this.checkBoxSmartTotems = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxAirTotem = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxWaterTotem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxFireTotem = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.linkLabelSeeSharp = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxHumanizerCastDelay = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownHumanizerCastDelayMax = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHumanizerCastDelayMin = new System.Windows.Forms.NumericUpDown();
            this.checkBoxHamanizerErrors = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownRestHealPercent = new System.Windows.Forms.NumericUpDown();
            this.checkBoxRestUseHeal = new System.Windows.Forms.CheckBox();
            this.textBoxRestDrinkname = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxRestFoodname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownKeepAboveManaPercentCombat = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.numericUpDownCombatLesserHealPercentHP = new System.Windows.Forms.NumericUpDown();
            this.checkBoxCombatUseLesserHeal = new System.Windows.Forms.CheckBox();
            this.checkBoxCombatUseStormstrike = new System.Windows.Forms.CheckBox();
            this.checkBoxCombatReapplyLightningShield = new System.Windows.Forms.CheckBox();
            this.checkBoxCombatApplyFlameshock = new System.Windows.Forms.CheckBox();
            this.checkBoxCombatAutoAttack = new System.Windows.Forms.CheckBox();
            this.checkBoxCombatUseShockSpells = new System.Windows.Forms.CheckBox();
            this.comboBoxCombatPreferedShock = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.checkBoxCombatUseHeal = new System.Windows.Forms.CheckBox();
            this.numericUpDownCombatHealHpPercent = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBoxBuffLightningShield = new System.Windows.Forms.CheckBox();
            this.checkBoxUseWeaponBuff = new System.Windows.Forms.CheckBox();
            this.checkBoxBuffWaterWalking = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxWeaponBuff = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxNoPullIfTargetSkull = new System.Windows.Forms.CheckBox();
            this.numericUpDownNoPullIfTargerLevelGreater = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numericUpDownNoPullIfTargetLevelLess = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxPullingSpell = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownPullRange = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonShowCCLog = new System.Windows.Forms.Button();
            this.buttonWelcomeMessageShow = new System.Windows.Forms.Button();
            this.buttonReportBugFormShow = new System.Windows.Forms.Button();
            this.buttonLoadCCDefaultConfigs = new System.Windows.Forms.Button();
            this.comboBoxCCLoadSettings = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxCCLanguage = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.labelAboutVersion = new System.Windows.Forms.Label();
            this.linkLabelEmu = new System.Windows.Forms.LinkLabel();
            this.linkLabelZzuk = new System.Windows.Forms.LinkLabel();
            this.buttonAboutForumThread = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxGroupHealOnly = new System.Windows.Forms.CheckBox();
            this.checkBoxUseGroupHeals = new System.Windows.Forms.CheckBox();
            this.buttonGroupHealSettings = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExitGUI)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTotemRange)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumanizerCastDelayMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumanizerCastDelayMin)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRestHealPercent)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKeepAboveManaPercentCombat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombatLesserHealPercentHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombatHealHpPercent)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargerLevelGreater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargetLevelLess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPullRange)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonClose.Location = new System.Drawing.Point(585, 9);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(244, 26);
            this.buttonClose.TabIndex = 0;
            this.buttonClose.Text = "Save Changes and Close UI";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.DimGray;
            this.panelTop.Controls.Add(this.pictureBoxMinimize);
            this.panelTop.Controls.Add(this.labelCCName);
            this.panelTop.Controls.Add(this.pictureBoxExitGUI);
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(841, 28);
            this.panelTop.TabIndex = 2;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // pictureBoxMinimize
            // 
            this.pictureBoxMinimize.Image = global::ShamanCCv2Dev.Properties.Resources.miniboxpixel;
            this.pictureBoxMinimize.Location = new System.Drawing.Point(783, 2);
            this.pictureBoxMinimize.Name = "pictureBoxMinimize";
            this.pictureBoxMinimize.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMinimize.TabIndex = 3;
            this.pictureBoxMinimize.TabStop = false;
            this.pictureBoxMinimize.Click += new System.EventHandler(this.pictureBoxMinimize_Click);
            // 
            // labelCCName
            // 
            this.labelCCName.AutoSize = true;
            this.labelCCName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCCName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelCCName.Location = new System.Drawing.Point(2, 5);
            this.labelCCName.Name = "labelCCName";
            this.labelCCName.Size = new System.Drawing.Size(340, 18);
            this.labelCCName.TabIndex = 2;
            this.labelCCName.Text = "zShaman [Enhancement] - A Zzukbot V2 CC";
            // 
            // pictureBoxExitGUI
            // 
            this.pictureBoxExitGUI.Image = global::ShamanCCv2Dev.Properties.Resources.zRed;
            this.pictureBoxExitGUI.Location = new System.Drawing.Point(813, 2);
            this.pictureBoxExitGUI.Name = "pictureBoxExitGUI";
            this.pictureBoxExitGUI.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxExitGUI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxExitGUI.TabIndex = 1;
            this.pictureBoxExitGUI.TabStop = false;
            this.pictureBoxExitGUI.Click += new System.EventHandler(this.pictureBoxExitGUI_Click);
            this.pictureBoxExitGUI.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxExitGUI_MouseClick);
            // 
            // comboBoxEarthTotem
            // 
            this.comboBoxEarthTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEarthTotem.FormattingEnabled = true;
            this.comboBoxEarthTotem.Items.AddRange(new object[] {
            "Stoneskin",
            "Earthbind",
            "Stoneclaw",
            "Stregnth of Earth"});
            this.comboBoxEarthTotem.Location = new System.Drawing.Point(54, 51);
            this.comboBoxEarthTotem.Name = "comboBoxEarthTotem";
            this.comboBoxEarthTotem.Size = new System.Drawing.Size(105, 21);
            this.comboBoxEarthTotem.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.checkBoxUseEarthTotem);
            this.groupBox1.Controls.Add(this.checkBoxUseAirTotem);
            this.groupBox1.Controls.Add(this.checkBoxUseWaterTotem);
            this.groupBox1.Controls.Add(this.checkBoxUseFireTotem);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.numericUpDownTotemRange);
            this.groupBox1.Controls.Add(this.checkBoxSmartTotems);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBoxAirTotem);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxWaterTotem);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxFireTotem);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxEarthTotem);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox1.Location = new System.Drawing.Point(6, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 221);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Totems";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Silver;
            this.label20.Location = new System.Drawing.Point(12, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(181, 16);
            this.label20.TabIndex = 20;
            this.label20.Text = "Type            Totem     Enabled";
            // 
            // checkBoxUseEarthTotem
            // 
            this.checkBoxUseEarthTotem.AutoSize = true;
            this.checkBoxUseEarthTotem.Checked = true;
            this.checkBoxUseEarthTotem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseEarthTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseEarthTotem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseEarthTotem.Location = new System.Drawing.Point(165, 54);
            this.checkBoxUseEarthTotem.Name = "checkBoxUseEarthTotem";
            this.checkBoxUseEarthTotem.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseEarthTotem.TabIndex = 19;
            this.checkBoxUseEarthTotem.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseAirTotem
            // 
            this.checkBoxUseAirTotem.AutoSize = true;
            this.checkBoxUseAirTotem.Checked = true;
            this.checkBoxUseAirTotem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseAirTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseAirTotem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseAirTotem.Location = new System.Drawing.Point(165, 135);
            this.checkBoxUseAirTotem.Name = "checkBoxUseAirTotem";
            this.checkBoxUseAirTotem.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseAirTotem.TabIndex = 18;
            this.checkBoxUseAirTotem.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseWaterTotem
            // 
            this.checkBoxUseWaterTotem.AutoSize = true;
            this.checkBoxUseWaterTotem.Checked = true;
            this.checkBoxUseWaterTotem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseWaterTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseWaterTotem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseWaterTotem.Location = new System.Drawing.Point(165, 108);
            this.checkBoxUseWaterTotem.Name = "checkBoxUseWaterTotem";
            this.checkBoxUseWaterTotem.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseWaterTotem.TabIndex = 17;
            this.checkBoxUseWaterTotem.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseFireTotem
            // 
            this.checkBoxUseFireTotem.AutoSize = true;
            this.checkBoxUseFireTotem.Checked = true;
            this.checkBoxUseFireTotem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseFireTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseFireTotem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseFireTotem.Location = new System.Drawing.Point(165, 81);
            this.checkBoxUseFireTotem.Name = "checkBoxUseFireTotem";
            this.checkBoxUseFireTotem.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseFireTotem.TabIndex = 16;
            this.checkBoxUseFireTotem.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(12, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Max Range:";
            // 
            // numericUpDownTotemRange
            // 
            this.numericUpDownTotemRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownTotemRange.Location = new System.Drawing.Point(96, 181);
            this.numericUpDownTotemRange.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownTotemRange.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTotemRange.Name = "numericUpDownTotemRange";
            this.numericUpDownTotemRange.Size = new System.Drawing.Size(73, 22);
            this.numericUpDownTotemRange.TabIndex = 14;
            this.numericUpDownTotemRange.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            // 
            // checkBoxSmartTotems
            // 
            this.checkBoxSmartTotems.AutoSize = true;
            this.checkBoxSmartTotems.Checked = true;
            this.checkBoxSmartTotems.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSmartTotems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSmartTotems.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxSmartTotems.Location = new System.Drawing.Point(15, 160);
            this.checkBoxSmartTotems.Name = "checkBoxSmartTotems";
            this.checkBoxSmartTotems.Size = new System.Drawing.Size(111, 20);
            this.checkBoxSmartTotems.TabIndex = 11;
            this.checkBoxSmartTotems.Text = "Smart Totems";
            this.checkBoxSmartTotems.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label4.Location = new System.Drawing.Point(27, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Air";
            // 
            // comboBoxAirTotem
            // 
            this.comboBoxAirTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAirTotem.FormattingEnabled = true;
            this.comboBoxAirTotem.Items.AddRange(new object[] {
            "Grounding",
            "Nature Resistance",
            "Windfurry",
            "Sentry"});
            this.comboBoxAirTotem.Location = new System.Drawing.Point(54, 132);
            this.comboBoxAirTotem.Name = "comboBoxAirTotem";
            this.comboBoxAirTotem.Size = new System.Drawing.Size(105, 21);
            this.comboBoxAirTotem.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(12, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Water";
            // 
            // comboBoxWaterTotem
            // 
            this.comboBoxWaterTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxWaterTotem.FormattingEnabled = true;
            this.comboBoxWaterTotem.Items.AddRange(new object[] {
            "Mana Spring",
            "Fire Resistance",
            "Healing Stream",
            "Disease Cleansing",
            "Poison Cleansing"});
            this.comboBoxWaterTotem.Location = new System.Drawing.Point(54, 105);
            this.comboBoxWaterTotem.Name = "comboBoxWaterTotem";
            this.comboBoxWaterTotem.Size = new System.Drawing.Size(105, 21);
            this.comboBoxWaterTotem.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label2.Location = new System.Drawing.Point(20, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Fire";
            // 
            // comboBoxFireTotem
            // 
            this.comboBoxFireTotem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFireTotem.FormattingEnabled = true;
            this.comboBoxFireTotem.Items.AddRange(new object[] {
            "Searing",
            "Fire Nova",
            "Magma",
            "Flametongue",
            "Frost Resistance"});
            this.comboBoxFireTotem.Location = new System.Drawing.Point(54, 78);
            this.comboBoxFireTotem.Name = "comboBoxFireTotem";
            this.comboBoxFireTotem.Size = new System.Drawing.Size(105, 21);
            this.comboBoxFireTotem.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Earth";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Gray;
            this.label24.Location = new System.Drawing.Point(12, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(183, 16);
            this.label24.TabIndex = 21;
            this.label24.Text = "_________________________";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Location = new System.Drawing.Point(0, 512);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(848, 41);
            this.panel1.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(408, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(171, 16);
            this.label19.TabIndex = 6;
            this.label19.Text = "Click when you\'re done ---->";
            // 
            // linkLabelSeeSharp
            // 
            this.linkLabelSeeSharp.AutoSize = true;
            this.linkLabelSeeSharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelSeeSharp.LinkColor = System.Drawing.Color.White;
            this.linkLabelSeeSharp.Location = new System.Drawing.Point(100, 207);
            this.linkLabelSeeSharp.Name = "linkLabelSeeSharp";
            this.linkLabelSeeSharp.Size = new System.Drawing.Size(89, 20);
            this.linkLabelSeeSharp.TabIndex = 4;
            this.linkLabelSeeSharp.TabStop = true;
            this.linkLabelSeeSharp.Text = "SeeSharp";
            this.linkLabelSeeSharp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSeeSharp_LinkClicked);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label18.Location = new System.Drawing.Point(6, 208);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 18);
            this.label18.TabIndex = 3;
            this.label18.Text = "Created By:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxHumanizerCastDelay);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.numericUpDownHumanizerCastDelayMax);
            this.groupBox2.Controls.Add(this.numericUpDownHumanizerCastDelayMin);
            this.groupBox2.Controls.Add(this.checkBoxHamanizerErrors);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox2.Location = new System.Drawing.Point(210, 397);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 109);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Humanizer";
            // 
            // checkBoxHumanizerCastDelay
            // 
            this.checkBoxHumanizerCastDelay.AutoSize = true;
            this.checkBoxHumanizerCastDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHumanizerCastDelay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHumanizerCastDelay.Location = new System.Drawing.Point(6, 51);
            this.checkBoxHumanizerCastDelay.Name = "checkBoxHumanizerCastDelay";
            this.checkBoxHumanizerCastDelay.Size = new System.Drawing.Size(123, 20);
            this.checkBoxHumanizerCastDelay.TabIndex = 13;
            this.checkBoxHumanizerCastDelay.Text = "Cast Delay(NYI)";
            this.checkBoxHumanizerCastDelay.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(156, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "MS";
            // 
            // numericUpDownHumanizerCastDelayMax
            // 
            this.numericUpDownHumanizerCastDelayMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownHumanizerCastDelayMax.Location = new System.Drawing.Point(90, 76);
            this.numericUpDownHumanizerCastDelayMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownHumanizerCastDelayMax.Name = "numericUpDownHumanizerCastDelayMax";
            this.numericUpDownHumanizerCastDelayMax.Size = new System.Drawing.Size(60, 22);
            this.numericUpDownHumanizerCastDelayMax.TabIndex = 15;
            this.numericUpDownHumanizerCastDelayMax.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // numericUpDownHumanizerCastDelayMin
            // 
            this.numericUpDownHumanizerCastDelayMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownHumanizerCastDelayMin.Location = new System.Drawing.Point(6, 76);
            this.numericUpDownHumanizerCastDelayMin.Name = "numericUpDownHumanizerCastDelayMin";
            this.numericUpDownHumanizerCastDelayMin.Size = new System.Drawing.Size(57, 22);
            this.numericUpDownHumanizerCastDelayMin.TabIndex = 14;
            this.numericUpDownHumanizerCastDelayMin.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // checkBoxHamanizerErrors
            // 
            this.checkBoxHamanizerErrors.AutoSize = true;
            this.checkBoxHamanizerErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHamanizerErrors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHamanizerErrors.Location = new System.Drawing.Point(6, 25);
            this.checkBoxHamanizerErrors.Name = "checkBoxHamanizerErrors";
            this.checkBoxHamanizerErrors.Size = new System.Drawing.Size(146, 20);
            this.checkBoxHamanizerErrors.TabIndex = 12;
            this.checkBoxHamanizerErrors.Text = "Make Human Errors";
            this.checkBoxHamanizerErrors.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(69, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "-";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownRestHealPercent);
            this.groupBox3.Controls.Add(this.checkBoxRestUseHeal);
            this.groupBox3.Controls.Add(this.textBoxRestDrinkname);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.textBoxRestFoodname);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox3.Location = new System.Drawing.Point(6, 381);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(198, 125);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resting";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // numericUpDownRestHealPercent
            // 
            this.numericUpDownRestHealPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownRestHealPercent.Location = new System.Drawing.Point(125, 88);
            this.numericUpDownRestHealPercent.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownRestHealPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRestHealPercent.Name = "numericUpDownRestHealPercent";
            this.numericUpDownRestHealPercent.Size = new System.Drawing.Size(55, 22);
            this.numericUpDownRestHealPercent.TabIndex = 19;
            this.numericUpDownRestHealPercent.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // checkBoxRestUseHeal
            // 
            this.checkBoxRestUseHeal.AutoSize = true;
            this.checkBoxRestUseHeal.Checked = true;
            this.checkBoxRestUseHeal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRestUseHeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRestUseHeal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxRestUseHeal.Location = new System.Drawing.Point(10, 90);
            this.checkBoxRestUseHeal.Name = "checkBoxRestUseHeal";
            this.checkBoxRestUseHeal.Size = new System.Drawing.Size(119, 20);
            this.checkBoxRestUseHeal.TabIndex = 16;
            this.checkBoxRestUseHeal.Text = "Heal if HP % <=";
            this.checkBoxRestUseHeal.UseVisualStyleBackColor = true;
            // 
            // textBoxRestDrinkname
            // 
            this.textBoxRestDrinkname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRestDrinkname.Location = new System.Drawing.Point(53, 54);
            this.textBoxRestDrinkname.Name = "textBoxRestDrinkname";
            this.textBoxRestDrinkname.Size = new System.Drawing.Size(127, 22);
            this.textBoxRestDrinkname.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(7, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Drink:";
            // 
            // textBoxRestFoodname
            // 
            this.textBoxRestFoodname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRestFoodname.Location = new System.Drawing.Point(53, 23);
            this.textBoxRestFoodname.Name = "textBoxRestFoodname";
            this.textBoxRestFoodname.Size = new System.Drawing.Size(127, 22);
            this.textBoxRestFoodname.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(7, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Food:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.numericUpDownKeepAboveManaPercentCombat);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.numericUpDownCombatLesserHealPercentHP);
            this.groupBox4.Controls.Add(this.checkBoxCombatUseLesserHeal);
            this.groupBox4.Controls.Add(this.checkBoxCombatUseStormstrike);
            this.groupBox4.Controls.Add(this.checkBoxCombatReapplyLightningShield);
            this.groupBox4.Controls.Add(this.checkBoxCombatApplyFlameshock);
            this.groupBox4.Controls.Add(this.checkBoxCombatAutoAttack);
            this.groupBox4.Controls.Add(this.checkBoxCombatUseShockSpells);
            this.groupBox4.Controls.Add(this.comboBoxCombatPreferedShock);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.checkBoxCombatUseHeal);
            this.groupBox4.Controls.Add(this.numericUpDownCombatHealHpPercent);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox4.Location = new System.Drawing.Point(409, 36);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(206, 328);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Combat";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(6, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 16);
            this.label10.TabIndex = 42;
            this.label10.Text = "For Healing Spells.";
            // 
            // numericUpDownKeepAboveManaPercentCombat
            // 
            this.numericUpDownKeepAboveManaPercentCombat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownKeepAboveManaPercentCombat.Location = new System.Drawing.Point(93, 248);
            this.numericUpDownKeepAboveManaPercentCombat.Maximum = new decimal(new int[] {
            65,
            0,
            0,
            0});
            this.numericUpDownKeepAboveManaPercentCombat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownKeepAboveManaPercentCombat.Name = "numericUpDownKeepAboveManaPercentCombat";
            this.numericUpDownKeepAboveManaPercentCombat.Size = new System.Drawing.Size(44, 22);
            this.numericUpDownKeepAboveManaPercentCombat.TabIndex = 41;
            this.numericUpDownKeepAboveManaPercentCombat.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label26.Location = new System.Drawing.Point(138, 251);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 16);
            this.label26.TabIndex = 40;
            this.label26.Text = "% Mana";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label25.Location = new System.Drawing.Point(6, 251);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 16);
            this.label25.TabIndex = 39;
            this.label25.Text = "Save around ";
            // 
            // numericUpDownCombatLesserHealPercentHP
            // 
            this.numericUpDownCombatLesserHealPercentHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownCombatLesserHealPercentHP.Location = new System.Drawing.Point(156, 159);
            this.numericUpDownCombatLesserHealPercentHP.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownCombatLesserHealPercentHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCombatLesserHealPercentHP.Name = "numericUpDownCombatLesserHealPercentHP";
            this.numericUpDownCombatLesserHealPercentHP.Size = new System.Drawing.Size(44, 22);
            this.numericUpDownCombatLesserHealPercentHP.TabIndex = 38;
            this.numericUpDownCombatLesserHealPercentHP.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // checkBoxCombatUseLesserHeal
            // 
            this.checkBoxCombatUseLesserHeal.AutoSize = true;
            this.checkBoxCombatUseLesserHeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatUseLesserHeal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatUseLesserHeal.Location = new System.Drawing.Point(6, 160);
            this.checkBoxCombatUseLesserHeal.Name = "checkBoxCombatUseLesserHeal";
            this.checkBoxCombatUseLesserHeal.Size = new System.Drawing.Size(151, 20);
            this.checkBoxCombatUseLesserHeal.TabIndex = 37;
            this.checkBoxCombatUseLesserHeal.Text = "Lesser Heal HP% <=";
            this.checkBoxCombatUseLesserHeal.UseVisualStyleBackColor = true;
            // 
            // checkBoxCombatUseStormstrike
            // 
            this.checkBoxCombatUseStormstrike.AutoSize = true;
            this.checkBoxCombatUseStormstrike.Checked = true;
            this.checkBoxCombatUseStormstrike.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatUseStormstrike.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatUseStormstrike.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatUseStormstrike.Location = new System.Drawing.Point(6, 213);
            this.checkBoxCombatUseStormstrike.Name = "checkBoxCombatUseStormstrike";
            this.checkBoxCombatUseStormstrike.Size = new System.Drawing.Size(198, 20);
            this.checkBoxCombatUseStormstrike.TabIndex = 36;
            this.checkBoxCombatUseStormstrike.Text = "Use Stormstrike (lvl 40 talent)";
            this.checkBoxCombatUseStormstrike.UseVisualStyleBackColor = true;
            // 
            // checkBoxCombatReapplyLightningShield
            // 
            this.checkBoxCombatReapplyLightningShield.AutoSize = true;
            this.checkBoxCombatReapplyLightningShield.Checked = true;
            this.checkBoxCombatReapplyLightningShield.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatReapplyLightningShield.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatReapplyLightningShield.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatReapplyLightningShield.Location = new System.Drawing.Point(6, 187);
            this.checkBoxCombatReapplyLightningShield.Name = "checkBoxCombatReapplyLightningShield";
            this.checkBoxCombatReapplyLightningShield.Size = new System.Drawing.Size(180, 20);
            this.checkBoxCombatReapplyLightningShield.TabIndex = 35;
            this.checkBoxCombatReapplyLightningShield.Text = "Re-apply Lightning Shield";
            this.checkBoxCombatReapplyLightningShield.UseVisualStyleBackColor = true;
            // 
            // checkBoxCombatApplyFlameshock
            // 
            this.checkBoxCombatApplyFlameshock.AutoSize = true;
            this.checkBoxCombatApplyFlameshock.Checked = true;
            this.checkBoxCombatApplyFlameshock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatApplyFlameshock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatApplyFlameshock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatApplyFlameshock.Location = new System.Drawing.Point(6, 108);
            this.checkBoxCombatApplyFlameshock.Name = "checkBoxCombatApplyFlameshock";
            this.checkBoxCombatApplyFlameshock.Size = new System.Drawing.Size(169, 20);
            this.checkBoxCombatApplyFlameshock.TabIndex = 33;
            this.checkBoxCombatApplyFlameshock.Text = "Apply Flameshock DoT";
            this.checkBoxCombatApplyFlameshock.UseVisualStyleBackColor = true;
            // 
            // checkBoxCombatAutoAttack
            // 
            this.checkBoxCombatAutoAttack.AutoSize = true;
            this.checkBoxCombatAutoAttack.Checked = true;
            this.checkBoxCombatAutoAttack.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatAutoAttack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatAutoAttack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatAutoAttack.Location = new System.Drawing.Point(6, 26);
            this.checkBoxCombatAutoAttack.Name = "checkBoxCombatAutoAttack";
            this.checkBoxCombatAutoAttack.Size = new System.Drawing.Size(94, 20);
            this.checkBoxCombatAutoAttack.TabIndex = 31;
            this.checkBoxCombatAutoAttack.Text = "Auto Attack";
            this.checkBoxCombatAutoAttack.UseVisualStyleBackColor = true;
            // 
            // checkBoxCombatUseShockSpells
            // 
            this.checkBoxCombatUseShockSpells.AutoSize = true;
            this.checkBoxCombatUseShockSpells.Checked = true;
            this.checkBoxCombatUseShockSpells.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatUseShockSpells.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatUseShockSpells.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatUseShockSpells.Location = new System.Drawing.Point(6, 51);
            this.checkBoxCombatUseShockSpells.Name = "checkBoxCombatUseShockSpells";
            this.checkBoxCombatUseShockSpells.Size = new System.Drawing.Size(140, 20);
            this.checkBoxCombatUseShockSpells.TabIndex = 30;
            this.checkBoxCombatUseShockSpells.Text = "Use \'Shock\' Spells";
            this.checkBoxCombatUseShockSpells.UseVisualStyleBackColor = true;
            // 
            // comboBoxCombatPreferedShock
            // 
            this.comboBoxCombatPreferedShock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCombatPreferedShock.FormattingEnabled = true;
            this.comboBoxCombatPreferedShock.Items.AddRange(new object[] {
            "Eartrh",
            "Flame",
            "Frost"});
            this.comboBoxCombatPreferedShock.Location = new System.Drawing.Point(93, 77);
            this.comboBoxCombatPreferedShock.Name = "comboBoxCombatPreferedShock";
            this.comboBoxCombatPreferedShock.Size = new System.Drawing.Size(98, 21);
            this.comboBoxCombatPreferedShock.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(6, 78);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 28;
            this.label15.Text = "Filler Shock:";
            // 
            // checkBoxCombatUseHeal
            // 
            this.checkBoxCombatUseHeal.AutoSize = true;
            this.checkBoxCombatUseHeal.Checked = true;
            this.checkBoxCombatUseHeal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCombatUseHeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCombatUseHeal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxCombatUseHeal.Location = new System.Drawing.Point(6, 134);
            this.checkBoxCombatUseHeal.Name = "checkBoxCombatUseHeal";
            this.checkBoxCombatUseHeal.Size = new System.Drawing.Size(119, 20);
            this.checkBoxCombatUseHeal.TabIndex = 27;
            this.checkBoxCombatUseHeal.Text = "Heal if HP % <=";
            this.checkBoxCombatUseHeal.UseVisualStyleBackColor = true;
            // 
            // numericUpDownCombatHealHpPercent
            // 
            this.numericUpDownCombatHealHpPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownCombatHealHpPercent.Location = new System.Drawing.Point(126, 133);
            this.numericUpDownCombatHealHpPercent.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownCombatHealHpPercent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCombatHealHpPercent.Name = "numericUpDownCombatHealHpPercent";
            this.numericUpDownCombatHealHpPercent.Size = new System.Drawing.Size(44, 22);
            this.numericUpDownCombatHealHpPercent.TabIndex = 26;
            this.numericUpDownCombatHealHpPercent.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBox1);
            this.groupBox5.Controls.Add(this.checkBoxBuffLightningShield);
            this.groupBox5.Controls.Add(this.checkBoxUseWeaponBuff);
            this.groupBox5.Controls.Add(this.checkBoxBuffWaterWalking);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.comboBoxWeaponBuff);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox5.Location = new System.Drawing.Point(213, 36);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(190, 153);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Buffing";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBox1.Location = new System.Drawing.Point(9, 120);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(92, 20);
            this.checkBox1.TabIndex = 22;
            this.checkBox1.Text = "Ghost Wolf";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBoxBuffLightningShield
            // 
            this.checkBoxBuffLightningShield.AutoSize = true;
            this.checkBoxBuffLightningShield.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBuffLightningShield.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBuffLightningShield.Location = new System.Drawing.Point(9, 94);
            this.checkBoxBuffLightningShield.Name = "checkBoxBuffLightningShield";
            this.checkBoxBuffLightningShield.Size = new System.Drawing.Size(121, 20);
            this.checkBoxBuffLightningShield.TabIndex = 21;
            this.checkBoxBuffLightningShield.Text = "Lightning Shield";
            this.checkBoxBuffLightningShield.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseWeaponBuff
            // 
            this.checkBoxUseWeaponBuff.AutoSize = true;
            this.checkBoxUseWeaponBuff.Checked = true;
            this.checkBoxUseWeaponBuff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseWeaponBuff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseWeaponBuff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseWeaponBuff.Location = new System.Drawing.Point(159, 44);
            this.checkBoxUseWeaponBuff.Name = "checkBoxUseWeaponBuff";
            this.checkBoxUseWeaponBuff.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUseWeaponBuff.TabIndex = 20;
            this.checkBoxUseWeaponBuff.UseVisualStyleBackColor = true;
            // 
            // checkBoxBuffWaterWalking
            // 
            this.checkBoxBuffWaterWalking.AutoSize = true;
            this.checkBoxBuffWaterWalking.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBuffWaterWalking.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxBuffWaterWalking.Location = new System.Drawing.Point(9, 68);
            this.checkBoxBuffWaterWalking.Name = "checkBoxBuffWaterWalking";
            this.checkBoxBuffWaterWalking.Size = new System.Drawing.Size(115, 20);
            this.checkBoxBuffWaterWalking.TabIndex = 12;
            this.checkBoxBuffWaterWalking.Text = "Water Walking";
            this.checkBoxBuffWaterWalking.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(6, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 5;
            this.label11.Text = "Weapon Buff:";
            // 
            // comboBoxWeaponBuff
            // 
            this.comboBoxWeaponBuff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxWeaponBuff.FormattingEnabled = true;
            this.comboBoxWeaponBuff.Items.AddRange(new object[] {
            "Rockbiter Weapon",
            "Flametongue Weapon",
            "Frostbrand Weapon",
            "Windfury Weapon"});
            this.comboBoxWeaponBuff.Location = new System.Drawing.Point(6, 41);
            this.comboBoxWeaponBuff.Name = "comboBoxWeaponBuff";
            this.comboBoxWeaponBuff.Size = new System.Drawing.Size(144, 21);
            this.comboBoxWeaponBuff.TabIndex = 4;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.panel2);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.textBoxPullingSpell);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.numericUpDownPullRange);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox6.Location = new System.Drawing.Point(210, 192);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(193, 203);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pulling";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.panel2.Controls.Add(this.checkBoxNoPullIfTargetSkull);
            this.panel2.Controls.Add(this.numericUpDownNoPullIfTargerLevelGreater);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.numericUpDownNoPullIfTargetLevelLess);
            this.panel2.Location = new System.Drawing.Point(3, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(187, 95);
            this.panel2.TabIndex = 38;
            // 
            // checkBoxNoPullIfTargetSkull
            // 
            this.checkBoxNoPullIfTargetSkull.AutoSize = true;
            this.checkBoxNoPullIfTargetSkull.Checked = true;
            this.checkBoxNoPullIfTargetSkull.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNoPullIfTargetSkull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxNoPullIfTargetSkull.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxNoPullIfTargetSkull.Location = new System.Drawing.Point(12, 6);
            this.checkBoxNoPullIfTargetSkull.Name = "checkBoxNoPullIfTargetSkull";
            this.checkBoxNoPullIfTargetSkull.Size = new System.Drawing.Size(137, 20);
            this.checkBoxNoPullIfTargetSkull.TabIndex = 36;
            this.checkBoxNoPullIfTargetSkull.Text = "Target is ?? (Skull)";
            this.checkBoxNoPullIfTargetSkull.UseVisualStyleBackColor = true;
            // 
            // numericUpDownNoPullIfTargerLevelGreater
            // 
            this.numericUpDownNoPullIfTargerLevelGreater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownNoPullIfTargerLevelGreater.Location = new System.Drawing.Point(132, 30);
            this.numericUpDownNoPullIfTargerLevelGreater.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargerLevelGreater.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargerLevelGreater.Name = "numericUpDownNoPullIfTargerLevelGreater";
            this.numericUpDownNoPullIfTargerLevelGreater.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownNoPullIfTargerLevelGreater.TabIndex = 26;
            this.numericUpDownNoPullIfTargerLevelGreater.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label29.Location = new System.Drawing.Point(3, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 16);
            this.label29.TabIndex = 29;
            this.label29.Text = "Tar lvl is < yours by:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(3, 34);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(125, 16);
            this.label28.TabIndex = 27;
            this.label28.Text = "Tar lvl is > yours by:";
            // 
            // numericUpDownNoPullIfTargetLevelLess
            // 
            this.numericUpDownNoPullIfTargetLevelLess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownNoPullIfTargetLevelLess.Location = new System.Drawing.Point(132, 58);
            this.numericUpDownNoPullIfTargetLevelLess.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargetLevelLess.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownNoPullIfTargetLevelLess.Name = "numericUpDownNoPullIfTargetLevelLess";
            this.numericUpDownNoPullIfTargetLevelLess.Size = new System.Drawing.Size(42, 22);
            this.numericUpDownNoPullIfTargetLevelLess.TabIndex = 28;
            this.numericUpDownNoPullIfTargetLevelLess.Value = new decimal(new int[] {
            59,
            0,
            0,
            0});
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(9, 85);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(125, 16);
            this.label27.TabIndex = 25;
            this.label27.Text = "Dont Pull if: (NYI)";
            // 
            // textBoxPullingSpell
            // 
            this.textBoxPullingSpell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPullingSpell.Location = new System.Drawing.Point(75, 46);
            this.textBoxPullingSpell.Name = "textBoxPullingSpell";
            this.textBoxPullingSpell.Size = new System.Drawing.Size(112, 22);
            this.textBoxPullingSpell.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(7, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 16);
            this.label14.TabIndex = 22;
            this.label14.Text = "Pull Spell";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(6, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 16);
            this.label12.TabIndex = 20;
            this.label12.Text = "Pull Range";
            // 
            // numericUpDownPullRange
            // 
            this.numericUpDownPullRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownPullRange.Location = new System.Drawing.Point(86, 20);
            this.numericUpDownPullRange.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownPullRange.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownPullRange.Name = "numericUpDownPullRange";
            this.numericUpDownPullRange.Size = new System.Drawing.Size(58, 22);
            this.numericUpDownPullRange.TabIndex = 20;
            this.numericUpDownPullRange.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonShowCCLog);
            this.groupBox7.Controls.Add(this.buttonWelcomeMessageShow);
            this.groupBox7.Controls.Add(this.buttonReportBugFormShow);
            this.groupBox7.Controls.Add(this.buttonLoadCCDefaultConfigs);
            this.groupBox7.Controls.Add(this.comboBoxCCLoadSettings);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.comboBoxCCLanguage);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox7.Location = new System.Drawing.Point(409, 370);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(420, 136);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Misc";
            // 
            // buttonShowCCLog
            // 
            this.buttonShowCCLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShowCCLog.ForeColor = System.Drawing.Color.Black;
            this.buttonShowCCLog.Location = new System.Drawing.Point(290, 98);
            this.buttonShowCCLog.Name = "buttonShowCCLog";
            this.buttonShowCCLog.Size = new System.Drawing.Size(124, 26);
            this.buttonShowCCLog.TabIndex = 42;
            this.buttonShowCCLog.Text = "CC Log";
            this.buttonShowCCLog.UseVisualStyleBackColor = true;
            this.buttonShowCCLog.Click += new System.EventHandler(this.buttonShowCCLog_Click);
            // 
            // buttonWelcomeMessageShow
            // 
            this.buttonWelcomeMessageShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonWelcomeMessageShow.ForeColor = System.Drawing.Color.Black;
            this.buttonWelcomeMessageShow.Location = new System.Drawing.Point(9, 98);
            this.buttonWelcomeMessageShow.Name = "buttonWelcomeMessageShow";
            this.buttonWelcomeMessageShow.Size = new System.Drawing.Size(275, 26);
            this.buttonWelcomeMessageShow.TabIndex = 41;
            this.buttonWelcomeMessageShow.Text = "Show Welcome";
            this.buttonWelcomeMessageShow.UseVisualStyleBackColor = true;
            this.buttonWelcomeMessageShow.Click += new System.EventHandler(this.buttonWelcomeMessageShow_Click);
            // 
            // buttonReportBugFormShow
            // 
            this.buttonReportBugFormShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReportBugFormShow.ForeColor = System.Drawing.Color.DarkRed;
            this.buttonReportBugFormShow.Location = new System.Drawing.Point(183, 21);
            this.buttonReportBugFormShow.Name = "buttonReportBugFormShow";
            this.buttonReportBugFormShow.Size = new System.Drawing.Size(231, 26);
            this.buttonReportBugFormShow.TabIndex = 37;
            this.buttonReportBugFormShow.Text = "Report a Bug!";
            this.buttonReportBugFormShow.UseVisualStyleBackColor = true;
            this.buttonReportBugFormShow.Click += new System.EventHandler(this.buttonReportBugFormShow_Click);
            // 
            // buttonLoadCCDefaultConfigs
            // 
            this.buttonLoadCCDefaultConfigs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadCCDefaultConfigs.ForeColor = System.Drawing.Color.Black;
            this.buttonLoadCCDefaultConfigs.Location = new System.Drawing.Point(312, 48);
            this.buttonLoadCCDefaultConfigs.Name = "buttonLoadCCDefaultConfigs";
            this.buttonLoadCCDefaultConfigs.Size = new System.Drawing.Size(102, 26);
            this.buttonLoadCCDefaultConfigs.TabIndex = 34;
            this.buttonLoadCCDefaultConfigs.Text = "<---- Load";
            this.buttonLoadCCDefaultConfigs.UseVisualStyleBackColor = true;
            this.buttonLoadCCDefaultConfigs.Click += new System.EventHandler(this.buttonLoadCCDefaultConfigs_Click);
            // 
            // comboBoxCCLoadSettings
            // 
            this.comboBoxCCLoadSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCCLoadSettings.FormattingEnabled = true;
            this.comboBoxCCLoadSettings.Items.AddRange(new object[] {
            "Default (Regular Questing)",
            "Lowbie (Pre lvl 6)"});
            this.comboBoxCCLoadSettings.Location = new System.Drawing.Point(141, 52);
            this.comboBoxCCLoadSettings.Name = "comboBoxCCLoadSettings";
            this.comboBoxCCLoadSettings.Size = new System.Drawing.Size(165, 21);
            this.comboBoxCCLoadSettings.TabIndex = 33;
            this.comboBoxCCLoadSettings.Text = "Select a Setting";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(6, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(135, 16);
            this.label17.TabIndex = 32;
            this.label17.Text = "Load Default Settings";
            // 
            // comboBoxCCLanguage
            // 
            this.comboBoxCCLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCCLanguage.FormattingEnabled = true;
            this.comboBoxCCLanguage.Items.AddRange(new object[] {
            "English",
            "(More to come)"});
            this.comboBoxCCLanguage.Location = new System.Drawing.Point(97, 24);
            this.comboBoxCCLanguage.Name = "comboBoxCCLanguage";
            this.comboBoxCCLanguage.Size = new System.Drawing.Size(78, 21);
            this.comboBoxCCLanguage.TabIndex = 30;
            this.comboBoxCCLanguage.Text = "English";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(6, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 16);
            this.label16.TabIndex = 29;
            this.label16.Text = "CC Language";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Gray;
            this.label13.Location = new System.Drawing.Point(35, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(344, 16);
            this.label13.TabIndex = 43;
            this.label13.Text = "________________________________________________";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.labelAboutVersion);
            this.groupBox8.Controls.Add(this.linkLabelEmu);
            this.groupBox8.Controls.Add(this.linkLabelZzuk);
            this.groupBox8.Controls.Add(this.buttonAboutForumThread);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.linkLabelSeeSharp);
            this.groupBox8.Controls.Add(this.pictureBox1);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox8.Location = new System.Drawing.Point(623, 36);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(206, 328);
            this.groupBox8.TabIndex = 34;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "About";
            this.groupBox8.Enter += new System.EventHandler(this.groupBox8_Enter);
            // 
            // labelAboutVersion
            // 
            this.labelAboutVersion.AutoSize = true;
            this.labelAboutVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAboutVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelAboutVersion.Location = new System.Drawing.Point(28, 174);
            this.labelAboutVersion.Name = "labelAboutVersion";
            this.labelAboutVersion.Size = new System.Drawing.Size(106, 16);
            this.labelAboutVersion.TabIndex = 40;
            this.labelAboutVersion.Text = "VERSION TEXT";
            // 
            // linkLabelEmu
            // 
            this.linkLabelEmu.AutoSize = true;
            this.linkLabelEmu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelEmu.LinkColor = System.Drawing.Color.White;
            this.linkLabelEmu.Location = new System.Drawing.Point(149, 232);
            this.linkLabelEmu.Name = "linkLabelEmu";
            this.linkLabelEmu.Size = new System.Drawing.Size(35, 16);
            this.linkLabelEmu.TabIndex = 39;
            this.linkLabelEmu.TabStop = true;
            this.linkLabelEmu.Text = "Emu";
            this.linkLabelEmu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEmu_LinkClicked);
            // 
            // linkLabelZzuk
            // 
            this.linkLabelZzuk.AutoSize = true;
            this.linkLabelZzuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelZzuk.LinkColor = System.Drawing.Color.White;
            this.linkLabelZzuk.Location = new System.Drawing.Point(111, 232);
            this.linkLabelZzuk.Name = "linkLabelZzuk";
            this.linkLabelZzuk.Size = new System.Drawing.Size(36, 16);
            this.linkLabelZzuk.TabIndex = 38;
            this.linkLabelZzuk.TabStop = true;
            this.linkLabelZzuk.Text = "Zzuk";
            this.linkLabelZzuk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelZzuk_LinkClicked);
            // 
            // buttonAboutForumThread
            // 
            this.buttonAboutForumThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAboutForumThread.ForeColor = System.Drawing.Color.Black;
            this.buttonAboutForumThread.Location = new System.Drawing.Point(26, 272);
            this.buttonAboutForumThread.Name = "buttonAboutForumThread";
            this.buttonAboutForumThread.Size = new System.Drawing.Size(150, 26);
            this.buttonAboutForumThread.TabIndex = 37;
            this.buttonAboutForumThread.Text = "Forum Thread";
            this.buttonAboutForumThread.UseVisualStyleBackColor = true;
            this.buttonAboutForumThread.Click += new System.EventHandler(this.buttonAboutForumThread_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label21.Location = new System.Drawing.Point(6, 232);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(105, 16);
            this.label21.TabIndex = 5;
            this.label21.Text = "Special Thanks:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(26, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.checkBoxGroupHealOnly);
            this.groupBox9.Controls.Add(this.checkBoxUseGroupHeals);
            this.groupBox9.Controls.Add(this.buttonGroupHealSettings);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox9.Location = new System.Drawing.Point(6, 263);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(198, 113);
            this.groupBox9.TabIndex = 35;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Group Healing";
            // 
            // checkBoxGroupHealOnly
            // 
            this.checkBoxGroupHealOnly.AutoSize = true;
            this.checkBoxGroupHealOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxGroupHealOnly.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxGroupHealOnly.Location = new System.Drawing.Point(15, 45);
            this.checkBoxGroupHealOnly.Name = "checkBoxGroupHealOnly";
            this.checkBoxGroupHealOnly.Size = new System.Drawing.Size(126, 20);
            this.checkBoxGroupHealOnly.TabIndex = 41;
            this.checkBoxGroupHealOnly.Text = "Only Group Heal";
            this.checkBoxGroupHealOnly.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseGroupHeals
            // 
            this.checkBoxUseGroupHeals.AutoSize = true;
            this.checkBoxUseGroupHeals.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseGroupHeals.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseGroupHeals.Location = new System.Drawing.Point(15, 24);
            this.checkBoxUseGroupHeals.Name = "checkBoxUseGroupHeals";
            this.checkBoxUseGroupHeals.Size = new System.Drawing.Size(131, 20);
            this.checkBoxUseGroupHeals.TabIndex = 40;
            this.checkBoxUseGroupHeals.Text = "Use Group Heals";
            this.checkBoxUseGroupHeals.UseVisualStyleBackColor = true;
            // 
            // buttonGroupHealSettings
            // 
            this.buttonGroupHealSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGroupHealSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonGroupHealSettings.Location = new System.Drawing.Point(15, 73);
            this.buttonGroupHealSettings.Name = "buttonGroupHealSettings";
            this.buttonGroupHealSettings.Size = new System.Drawing.Size(165, 28);
            this.buttonGroupHealSettings.TabIndex = 39;
            this.buttonGroupHealSettings.Text = "Group Heal Settings";
            this.buttonGroupHealSettings.UseVisualStyleBackColor = true;
            this.buttonGroupHealSettings.Click += new System.EventHandler(this.buttonGroupHealSettings_Click);
            // 
            // CCGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(840, 553);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CCGui";
            this.Text = "CCGui";
            this.Load += new System.EventHandler(this.CCGui_Load);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExitGUI)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTotemRange)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumanizerCastDelayMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHumanizerCastDelayMin)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRestHealPercent)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKeepAboveManaPercentCombat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombatLesserHealPercentHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCombatHealHpPercent)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargerLevelGreater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoPullIfTargetLevelLess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPullRange)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.PictureBox pictureBoxExitGUI;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label labelCCName;
        private System.Windows.Forms.ComboBox comboBoxEarthTotem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxSmartTotems;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxAirTotem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxWaterTotem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxFireTotem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownTotemRange;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownHumanizerCastDelayMax;
        private System.Windows.Forms.NumericUpDown numericUpDownHumanizerCastDelayMin;
        private System.Windows.Forms.CheckBox checkBoxHumanizerCastDelay;
        private System.Windows.Forms.CheckBox checkBoxHamanizerErrors;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxRestFoodname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numericUpDownRestHealPercent;
        private System.Windows.Forms.CheckBox checkBoxRestUseHeal;
        private System.Windows.Forms.TextBox textBoxRestDrinkname;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxWeaponBuff;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkBoxBuffWaterWalking;
        private System.Windows.Forms.CheckBox checkBoxUseAirTotem;
        private System.Windows.Forms.CheckBox checkBoxUseWaterTotem;
        private System.Windows.Forms.CheckBox checkBoxUseFireTotem;
        private System.Windows.Forms.CheckBox checkBoxUseEarthTotem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDownPullRange;
        private System.Windows.Forms.TextBox textBoxPullingSpell;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkBoxUseWeaponBuff;
        private System.Windows.Forms.CheckBox checkBoxCombatUseHeal;
        private System.Windows.Forms.NumericUpDown numericUpDownCombatHealHpPercent;
        private System.Windows.Forms.ComboBox comboBoxCombatPreferedShock;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox checkBoxCombatUseShockSpells;
        private System.Windows.Forms.CheckBox checkBoxCombatAutoAttack;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox comboBoxCCLanguage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button buttonLoadCCDefaultConfigs;
        private System.Windows.Forms.ComboBox comboBoxCCLoadSettings;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.LinkLabel linkLabelSeeSharp;
        private System.Windows.Forms.CheckBox checkBoxCombatApplyFlameshock;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button buttonReportBugFormShow;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonAboutForumThread;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.LinkLabel linkLabelEmu;
        private System.Windows.Forms.LinkLabel linkLabelZzuk;
        private System.Windows.Forms.Label labelAboutVersion;
        private System.Windows.Forms.CheckBox checkBoxBuffLightningShield;
        private System.Windows.Forms.CheckBox checkBoxCombatReapplyLightningShield;
        private System.Windows.Forms.PictureBox pictureBoxMinimize;
        private System.Windows.Forms.NumericUpDown numericUpDownCombatLesserHealPercentHP;
        private System.Windows.Forms.CheckBox checkBoxCombatUseLesserHeal;
        private System.Windows.Forms.CheckBox checkBoxCombatUseStormstrike;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDownKeepAboveManaPercentCombat;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown numericUpDownNoPullIfTargetLevelLess;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numericUpDownNoPullIfTargerLevelGreater;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox checkBoxNoPullIfTargetSkull;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button buttonGroupHealSettings;
        private System.Windows.Forms.Button buttonShowCCLog;
        private System.Windows.Forms.Button buttonWelcomeMessageShow;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxUseGroupHeals;
        private System.Windows.Forms.CheckBox checkBoxGroupHealOnly;
    }
}