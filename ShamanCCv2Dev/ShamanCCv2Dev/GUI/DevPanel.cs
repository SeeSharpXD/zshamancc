﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

using ShamanCCv2Dev.MoreMethods;

using ZzukBot.Objects;
using ZzukBot.Game.Statics;

namespace ShamanCCv2Dev.GUI
{
    public partial class DevPanel : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public DevPanel()
        {
            InitializeComponent();
        }

        private void buttonPrintToLog_Click(object sender, EventArgs e)
        {
            zShamanHelpers.PrintAurasToGame();
        }

        private void buttonRefreshAuras_Click(object sender, EventArgs e)
        {
            List<int> aurasListTemp = ObjectManager.Instance.Player.Auras;
            richTextBoxAuras.Text = "";
            richTextBoxAuras.Text += "Auras: " + Environment.NewLine;
            for (int i = 0; i < aurasListTemp.Count; i += 1)
            {
                richTextBoxAuras.Text += aurasListTemp[i].ToString() + Environment.NewLine;
            }
            richTextBoxAuras.Text += "----Done----" + Environment.NewLine;
        }

        private void pictureBoxClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void buttonPrintProgramDirToLog_Click(object sender, EventArgs e)
        {
            Helpers.PrintToChat("zShaman Working Directory");
            Helpers.PrintToChat(Directory.GetCurrentDirectory());
        }

        private void buttonPrintTargetInfo_Click(object sender, EventArgs e)
        {
            WoWUnit tar = ObjectManager.Instance.Target;
            if (tar == null)
            {
                Helpers.PrintToChat("----No Target----");
            }
            else
            {
                Helpers.PrintToChat("----Target Info----");
                Helpers.PrintToChat("Name: " + tar.Name);
                Helpers.PrintToChat("Type: " + tar.CreatureType.ToString());
                Helpers.PrintToChat("Is Player?: " + tar.IsPlayer.ToString());
                Helpers.PrintToChat("Position: " + tar.Position.ToString());
                if (!tar.IsPlayer)
                {
                    Helpers.PrintToChat("ID: " + tar.Id.ToString());
                    Helpers.PrintToChat("GUID: " + tar.Guid.ToString());
                    Helpers.PrintToChat("Reaction to player: " + tar.Reaction.ToString());
                }
                Helpers.PrintToChat("Faction ID: " + tar.FactionId.ToString());
                Helpers.PrintToChat("");
                Helpers.PrintToChat("Auras/Buffs:");
                for (int i = 0; i < tar.Auras.Count; i++) // Loop with for.
                {
                    Helpers.PrintToChat(tar.Auras[i].ToString());
                }
                Helpers.PrintToChat("");
                Helpers.PrintToChat("Debuffs:");
                for (int i = 0; i < tar.Debuffs.Count; i++) // Loop with for.
                {
                    Helpers.PrintToChat(tar.Debuffs[i].ToString());
                }
                Helpers.PrintToChat("");
                Helpers.PrintToChat("Stats:");
                Helpers.PrintToChat("HP: " + tar.Health.ToString() + "/" + tar.MaxHealth.ToString() + " [" + tar.HealthPercent.ToString() + "%]");
                Helpers.PrintToChat("Mana: " + tar.Mana.ToString() + "/" + tar.MaxMana.ToString() + " [" + tar.ManaPercent.ToString() + "%]");
                Helpers.PrintToChat("Rage: " + tar.Rage.ToString());
                Helpers.PrintToChat("Energy: " + tar.Energy.ToString());
                Helpers.PrintToChat("-------------");
            }
        }

        private void buttonPrintPlayerInfo_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string test;
            try
            {
                test = Inventory.Instance.GetItem(textBoxItemNameToCheck.Text).Id.ToString();
            }
            catch
            {
                test = "ERROR";
            }
            Helpers.PrintToChat(test);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string test;
            try
            {
                test = Inventory.Instance.GetItemCount(textBoxItemNameToCheck.Text).ToString();
            }
            catch
            {
                test = "ERROR";
            }
            Helpers.PrintToChat(test);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string test;
            try
            {
                test = Inventory.Instance.GetItemCount(Inventory.Instance.GetItem(textBoxItemNameToCheck.Text).Id).ToString();
            }
            catch
            {
                test = "ERROR";
            }
            Helpers.PrintToChat(test);
        }
    }
}
