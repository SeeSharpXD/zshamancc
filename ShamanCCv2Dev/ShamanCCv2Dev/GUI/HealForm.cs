﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ShamanCCv2Dev.Constants;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;
using ZzukBot.Settings;
using ShamanCCv2Dev.MoreMethods;
using ShamanCCv2Dev.Constants;

namespace ShamanCCv2Dev.GUI
{
    public partial class HealForm : Form
    {
        ShamanConstants shamanConstanctsObj = new ShamanConstants();
        public zShaman parentCCObj;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public HealForm()
        {
            InitializeComponent();
        }

        private void HealForm_Load(object sender, EventArgs e)
        {
            refreshFormPlayers();
        }

        private void buttonSaveGroupHealSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
            try
            {
                OptionManager.Get(shamanConstanctsObj.ccName + "GroupHeal").SaveToJson(parentCCObj.settingsGroupHeal);
                Helpers.PrintToChat("GroupHeal Settings Saved to JSON");
            }
            catch
            {
                Helpers.PrintToChat("Error saving GroupHeal Settings Saved to JSON");
            }
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void PreLoad()
        {
            try
            {
                Helpers.PrintToChat("Group Heal settings loaded to form");
            }
            catch
            {
                Helpers.PrintToChat("Error loading Group Heal settings to form");
            }
        }
        public void SaveSettings()
        {
            try
            {
                Helpers.PrintToChat("Group Heal settings saved to current session");
            }
            catch
            {
                Helpers.PrintToChat("Error Saving Groupheal Variables to current session");
            }
        }

        private void panel8_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void buttonRefreshGroup_Click(object sender, EventArgs e)
        {
            refreshFormPlayers();
        }

        public void refreshFormPlayers()
        {
            if (!ObjectManager.Instance.IsIngame)
            {
                return;
            }
            bool party1Exists = true;
            bool party2Exists = true;
            bool party3Exists = true;
            bool party4Exists = true;
            if (ObjectManager.Instance.Party1 == null)
            {
                party1Exists = false;
            }
            if (ObjectManager.Instance.Party2 == null)
            {
                party2Exists = false;
            }
            if (ObjectManager.Instance.Party3 == null)
            {
                party3Exists = false;
            }
            if (ObjectManager.Instance.Party4 == null)
            {
                party4Exists = false;
            }

            labelPlayernameYou.Text = ObjectManager.Instance.Player.Name;
            labelHPPercentYou.Text = ObjectManager.Instance.Player.Health.ToString() + " / " + ObjectManager.Instance.Player.MaxHealth.ToString();

            if (party1Exists)
            {
                labelPlayernameP2.Text = ObjectManager.Instance.Party1.Name;
                labelHPPercentP2.Text = ObjectManager.Instance.Party1.Health.ToString() + " / " + ObjectManager.Instance.Party1.MaxHealth.ToString();
            }
            else
            {
                labelPlayernameP2.Text = "NO ONE";
                labelHPPercentP2.Text = "";
            }
            if (party2Exists)
            {
                labelPlayernameP3.Text = ObjectManager.Instance.Party2.Name;
                labelHPPercentP3.Text = ObjectManager.Instance.Party2.Health.ToString() + " / " + ObjectManager.Instance.Party2.MaxHealth.ToString();
            }
            else
            {
                labelPlayernameP3.Text = "NO ONE";
                labelHPPercentP3.Text = "";
            }
            if (party3Exists)
            {
                labelPlayernameP4.Text = ObjectManager.Instance.Party3.Name;
                labelHPPercentP4.Text = ObjectManager.Instance.Party3.Health.ToString() + " / " + ObjectManager.Instance.Party3.MaxHealth.ToString();
            }
            else
            {
                labelPlayernameP4.Text = "NO ONE";
                labelHPPercentP4.Text = "";
            }
            if (party4Exists)
            {
                labelPlayernameP5.Text = ObjectManager.Instance.Party4.Name;
                labelHPPercentP5.Text = ObjectManager.Instance.Party4.Health.ToString() + " / " + ObjectManager.Instance.Party4.MaxHealth.ToString();
            }
            else
            {
                labelPlayernameP5.Text = "NO ONE";
                labelHPPercentP5.Text = "";
            }
        }
    }
}
