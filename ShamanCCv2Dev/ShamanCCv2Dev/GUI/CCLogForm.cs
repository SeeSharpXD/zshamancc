﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ShamanCCv2Dev.GUI
{
    public partial class CCLogForm : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public CCLogForm()
        {
            InitializeComponent();
        }

        private void pictureBoxExitGUI_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void richTextBoxLog_TextChanged(object sender, EventArgs e)
        {

        }

        public void UpdateLog(string textToAdd, string prefixText = "CC")
        {
            if (richTextBoxLog.Text.Length >= richTextBoxLog.MaxLength - 1000)
            {
                richTextBoxLog.Text = "";
            }
            richTextBoxLog.Text += textToAdd + Environment.NewLine;
            richTextBoxLog.SelectionStart = richTextBoxLog.Text.Length;
            richTextBoxLog.ScrollToCaret();
        }

        private void CCLogForm_Load(object sender, EventArgs e)
        {

        }
    }
}
