﻿namespace ShamanCCv2Dev.GUI
{
    partial class HealForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonRefreshGroup = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.checkBoxHealP4 = new System.Windows.Forms.CheckBox();
            this.labelHPPercentP5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelPlayernameP5 = new System.Windows.Forms.Label();
            this.comboBoxRoleP5 = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.checkBoxHealP3 = new System.Windows.Forms.CheckBox();
            this.labelHPPercentP4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelPlayernameP4 = new System.Windows.Forms.Label();
            this.comboBoxRoleP4 = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxHealP2 = new System.Windows.Forms.CheckBox();
            this.labelHPPercentP3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelPlayernameP3 = new System.Windows.Forms.Label();
            this.comboBoxRoleP3 = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxHealP1 = new System.Windows.Forms.CheckBox();
            this.labelHPPercentP2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelPlayernameP2 = new System.Windows.Forms.Label();
            this.comboBoxRoleP2 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxHealYou = new System.Windows.Forms.CheckBox();
            this.labelHPPercentYou = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelPlayernameYou = new System.Windows.Forms.Label();
            this.comboBoxRoleYou = new System.Windows.Forms.ComboBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.buttonSaveGroupHealSettings = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelFormName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxUseLesserHealingWave = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownLesserHealMana = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownLesserHealHP = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDownHealingWaveManaMin = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownHealingWaveHPMax = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxUseHealingWave = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.checkBoxUseChainHeal = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLesserHealMana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLesserHealHP)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealingWaveManaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealingWaveHPMax)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.panel1.Controls.Add(this.buttonRefreshGroup);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(318, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 493);
            this.panel1.TabIndex = 0;
            // 
            // buttonRefreshGroup
            // 
            this.buttonRefreshGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefreshGroup.Location = new System.Drawing.Point(15, 3);
            this.buttonRefreshGroup.Name = "buttonRefreshGroup";
            this.buttonRefreshGroup.Size = new System.Drawing.Size(271, 28);
            this.buttonRefreshGroup.TabIndex = 5;
            this.buttonRefreshGroup.Text = "Refresh Group";
            this.buttonRefreshGroup.UseVisualStyleBackColor = true;
            this.buttonRefreshGroup.Click += new System.EventHandler(this.buttonRefreshGroup_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel6.Controls.Add(this.checkBoxHealP4);
            this.panel6.Controls.Add(this.labelHPPercentP5);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.labelPlayernameP5);
            this.panel6.Controls.Add(this.comboBoxRoleP5);
            this.panel6.Location = new System.Drawing.Point(15, 379);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(270, 77);
            this.panel6.TabIndex = 4;
            // 
            // checkBoxHealP4
            // 
            this.checkBoxHealP4.AutoSize = true;
            this.checkBoxHealP4.Checked = true;
            this.checkBoxHealP4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealP4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealP4.Location = new System.Drawing.Point(146, 37);
            this.checkBoxHealP4.Name = "checkBoxHealP4";
            this.checkBoxHealP4.Size = new System.Drawing.Size(98, 20);
            this.checkBoxHealP4.TabIndex = 24;
            this.checkBoxHealP4.Text = "Heal Player";
            this.checkBoxHealP4.UseVisualStyleBackColor = true;
            // 
            // labelHPPercentP5
            // 
            this.labelHPPercentP5.AutoSize = true;
            this.labelHPPercentP5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPPercentP5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelHPPercentP5.Location = new System.Drawing.Point(43, 38);
            this.labelHPPercentP5.Name = "labelHPPercentP5";
            this.labelHPPercentP5.Size = new System.Drawing.Size(39, 16);
            this.labelHPPercentP5.TabIndex = 5;
            this.labelHPPercentP5.Text = "5,255";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(13, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 16);
            this.label15.TabIndex = 4;
            this.label15.Text = "HP:";
            // 
            // labelPlayernameP5
            // 
            this.labelPlayernameP5.AutoSize = true;
            this.labelPlayernameP5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayernameP5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelPlayernameP5.Location = new System.Drawing.Point(13, 6);
            this.labelPlayernameP5.Name = "labelPlayernameP5";
            this.labelPlayernameP5.Size = new System.Drawing.Size(99, 13);
            this.labelPlayernameP5.TabIndex = 2;
            this.labelPlayernameP5.Text = "Playername (P5)";
            // 
            // comboBoxRoleP5
            // 
            this.comboBoxRoleP5.FormattingEnabled = true;
            this.comboBoxRoleP5.Items.AddRange(new object[] {
            "Tank",
            "Heal",
            "DPS"});
            this.comboBoxRoleP5.Location = new System.Drawing.Point(168, 3);
            this.comboBoxRoleP5.Name = "comboBoxRoleP5";
            this.comboBoxRoleP5.Size = new System.Drawing.Size(99, 21);
            this.comboBoxRoleP5.TabIndex = 1;
            this.comboBoxRoleP5.Text = "Choose Role";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel5.Controls.Add(this.checkBoxHealP3);
            this.panel5.Controls.Add(this.labelHPPercentP4);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.labelPlayernameP4);
            this.panel5.Controls.Add(this.comboBoxRoleP4);
            this.panel5.Location = new System.Drawing.Point(15, 296);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(270, 77);
            this.panel5.TabIndex = 3;
            // 
            // checkBoxHealP3
            // 
            this.checkBoxHealP3.AutoSize = true;
            this.checkBoxHealP3.Checked = true;
            this.checkBoxHealP3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealP3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealP3.Location = new System.Drawing.Point(146, 38);
            this.checkBoxHealP3.Name = "checkBoxHealP3";
            this.checkBoxHealP3.Size = new System.Drawing.Size(98, 20);
            this.checkBoxHealP3.TabIndex = 24;
            this.checkBoxHealP3.Text = "Heal Player";
            this.checkBoxHealP3.UseVisualStyleBackColor = true;
            // 
            // labelHPPercentP4
            // 
            this.labelHPPercentP4.AutoSize = true;
            this.labelHPPercentP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPPercentP4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelHPPercentP4.Location = new System.Drawing.Point(43, 39);
            this.labelHPPercentP4.Name = "labelHPPercentP4";
            this.labelHPPercentP4.Size = new System.Drawing.Size(39, 16);
            this.labelHPPercentP4.TabIndex = 5;
            this.labelHPPercentP4.Text = "5,255";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(13, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "HP:";
            // 
            // labelPlayernameP4
            // 
            this.labelPlayernameP4.AutoSize = true;
            this.labelPlayernameP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayernameP4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelPlayernameP4.Location = new System.Drawing.Point(13, 6);
            this.labelPlayernameP4.Name = "labelPlayernameP4";
            this.labelPlayernameP4.Size = new System.Drawing.Size(99, 13);
            this.labelPlayernameP4.TabIndex = 2;
            this.labelPlayernameP4.Text = "Playername (P4)";
            // 
            // comboBoxRoleP4
            // 
            this.comboBoxRoleP4.FormattingEnabled = true;
            this.comboBoxRoleP4.Items.AddRange(new object[] {
            "Tank",
            "Heal",
            "DPS"});
            this.comboBoxRoleP4.Location = new System.Drawing.Point(168, 3);
            this.comboBoxRoleP4.Name = "comboBoxRoleP4";
            this.comboBoxRoleP4.Size = new System.Drawing.Size(99, 21);
            this.comboBoxRoleP4.TabIndex = 1;
            this.comboBoxRoleP4.Text = "Choose Role";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel4.Controls.Add(this.checkBoxHealP2);
            this.panel4.Controls.Add(this.labelHPPercentP3);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.labelPlayernameP3);
            this.panel4.Controls.Add(this.comboBoxRoleP3);
            this.panel4.Location = new System.Drawing.Point(15, 213);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(270, 77);
            this.panel4.TabIndex = 2;
            // 
            // checkBoxHealP2
            // 
            this.checkBoxHealP2.AutoSize = true;
            this.checkBoxHealP2.Checked = true;
            this.checkBoxHealP2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealP2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealP2.Location = new System.Drawing.Point(146, 34);
            this.checkBoxHealP2.Name = "checkBoxHealP2";
            this.checkBoxHealP2.Size = new System.Drawing.Size(98, 20);
            this.checkBoxHealP2.TabIndex = 24;
            this.checkBoxHealP2.Text = "Heal Player";
            this.checkBoxHealP2.UseVisualStyleBackColor = true;
            // 
            // labelHPPercentP3
            // 
            this.labelHPPercentP3.AutoSize = true;
            this.labelHPPercentP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPPercentP3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelHPPercentP3.Location = new System.Drawing.Point(43, 35);
            this.labelHPPercentP3.Name = "labelHPPercentP3";
            this.labelHPPercentP3.Size = new System.Drawing.Size(39, 16);
            this.labelHPPercentP3.TabIndex = 5;
            this.labelHPPercentP3.Text = "5,255";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(13, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 16);
            this.label11.TabIndex = 4;
            this.label11.Text = "HP:";
            // 
            // labelPlayernameP3
            // 
            this.labelPlayernameP3.AutoSize = true;
            this.labelPlayernameP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayernameP3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelPlayernameP3.Location = new System.Drawing.Point(13, 6);
            this.labelPlayernameP3.Name = "labelPlayernameP3";
            this.labelPlayernameP3.Size = new System.Drawing.Size(99, 13);
            this.labelPlayernameP3.TabIndex = 2;
            this.labelPlayernameP3.Text = "Playername (P3)";
            // 
            // comboBoxRoleP3
            // 
            this.comboBoxRoleP3.FormattingEnabled = true;
            this.comboBoxRoleP3.Items.AddRange(new object[] {
            "Tank",
            "Heal",
            "DPS"});
            this.comboBoxRoleP3.Location = new System.Drawing.Point(168, 3);
            this.comboBoxRoleP3.Name = "comboBoxRoleP3";
            this.comboBoxRoleP3.Size = new System.Drawing.Size(99, 21);
            this.comboBoxRoleP3.TabIndex = 1;
            this.comboBoxRoleP3.Text = "Choose Role";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel3.Controls.Add(this.checkBoxHealP1);
            this.panel3.Controls.Add(this.labelHPPercentP2);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.labelPlayernameP2);
            this.panel3.Controls.Add(this.comboBoxRoleP2);
            this.panel3.Location = new System.Drawing.Point(15, 130);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(270, 77);
            this.panel3.TabIndex = 1;
            // 
            // checkBoxHealP1
            // 
            this.checkBoxHealP1.AutoSize = true;
            this.checkBoxHealP1.Checked = true;
            this.checkBoxHealP1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealP1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealP1.Location = new System.Drawing.Point(146, 40);
            this.checkBoxHealP1.Name = "checkBoxHealP1";
            this.checkBoxHealP1.Size = new System.Drawing.Size(98, 20);
            this.checkBoxHealP1.TabIndex = 24;
            this.checkBoxHealP1.Text = "Heal Player";
            this.checkBoxHealP1.UseVisualStyleBackColor = true;
            // 
            // labelHPPercentP2
            // 
            this.labelHPPercentP2.AutoSize = true;
            this.labelHPPercentP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPPercentP2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelHPPercentP2.Location = new System.Drawing.Point(43, 41);
            this.labelHPPercentP2.Name = "labelHPPercentP2";
            this.labelHPPercentP2.Size = new System.Drawing.Size(39, 16);
            this.labelHPPercentP2.TabIndex = 5;
            this.labelHPPercentP2.Text = "5,255";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(13, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 16);
            this.label9.TabIndex = 4;
            this.label9.Text = "HP:";
            // 
            // labelPlayernameP2
            // 
            this.labelPlayernameP2.AutoSize = true;
            this.labelPlayernameP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayernameP2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelPlayernameP2.Location = new System.Drawing.Point(13, 6);
            this.labelPlayernameP2.Name = "labelPlayernameP2";
            this.labelPlayernameP2.Size = new System.Drawing.Size(99, 13);
            this.labelPlayernameP2.TabIndex = 2;
            this.labelPlayernameP2.Text = "Playername (P2)";
            // 
            // comboBoxRoleP2
            // 
            this.comboBoxRoleP2.FormattingEnabled = true;
            this.comboBoxRoleP2.Items.AddRange(new object[] {
            "Tank",
            "Heal",
            "DPS"});
            this.comboBoxRoleP2.Location = new System.Drawing.Point(168, 3);
            this.comboBoxRoleP2.Name = "comboBoxRoleP2";
            this.comboBoxRoleP2.Size = new System.Drawing.Size(99, 21);
            this.comboBoxRoleP2.TabIndex = 1;
            this.comboBoxRoleP2.Text = "Choose Role";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel2.Controls.Add(this.checkBoxHealYou);
            this.panel2.Controls.Add(this.labelHPPercentYou);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labelPlayernameYou);
            this.panel2.Controls.Add(this.comboBoxRoleYou);
            this.panel2.Location = new System.Drawing.Point(15, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 77);
            this.panel2.TabIndex = 0;
            // 
            // checkBoxHealYou
            // 
            this.checkBoxHealYou.AutoSize = true;
            this.checkBoxHealYou.Checked = true;
            this.checkBoxHealYou.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHealYou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHealYou.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxHealYou.Location = new System.Drawing.Point(146, 37);
            this.checkBoxHealYou.Name = "checkBoxHealYou";
            this.checkBoxHealYou.Size = new System.Drawing.Size(98, 20);
            this.checkBoxHealYou.TabIndex = 23;
            this.checkBoxHealYou.Text = "Heal Player";
            this.checkBoxHealYou.UseVisualStyleBackColor = true;
            // 
            // labelHPPercentYou
            // 
            this.labelHPPercentYou.AutoSize = true;
            this.labelHPPercentYou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPPercentYou.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelHPPercentYou.Location = new System.Drawing.Point(43, 38);
            this.labelHPPercentYou.Name = "labelHPPercentYou";
            this.labelHPPercentYou.Size = new System.Drawing.Size(39, 16);
            this.labelHPPercentYou.TabIndex = 3;
            this.labelHPPercentYou.Text = "5,255";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(13, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "HP:";
            // 
            // labelPlayernameYou
            // 
            this.labelPlayernameYou.AutoSize = true;
            this.labelPlayernameYou.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPlayernameYou.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelPlayernameYou.Location = new System.Drawing.Point(13, 3);
            this.labelPlayernameYou.Name = "labelPlayernameYou";
            this.labelPlayernameYou.Size = new System.Drawing.Size(106, 13);
            this.labelPlayernameYou.TabIndex = 1;
            this.labelPlayernameYou.Text = "Playername (You)";
            // 
            // comboBoxRoleYou
            // 
            this.comboBoxRoleYou.FormattingEnabled = true;
            this.comboBoxRoleYou.Items.AddRange(new object[] {
            "Tank",
            "Heal",
            "DPS"});
            this.comboBoxRoleYou.Location = new System.Drawing.Point(168, 0);
            this.comboBoxRoleYou.Name = "comboBoxRoleYou";
            this.comboBoxRoleYou.Size = new System.Drawing.Size(99, 21);
            this.comboBoxRoleYou.TabIndex = 0;
            this.comboBoxRoleYou.Text = "Choose Role";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.buttonSaveGroupHealSettings);
            this.panel7.Location = new System.Drawing.Point(-1, 505);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(618, 56);
            this.panel7.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(235, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(171, 16);
            this.label19.TabIndex = 7;
            this.label19.Text = "Click when you\'re done ---->";
            // 
            // buttonSaveGroupHealSettings
            // 
            this.buttonSaveGroupHealSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveGroupHealSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(60)))), ((int)(((byte)(0)))));
            this.buttonSaveGroupHealSettings.Location = new System.Drawing.Point(412, 9);
            this.buttonSaveGroupHealSettings.Name = "buttonSaveGroupHealSettings";
            this.buttonSaveGroupHealSettings.Size = new System.Drawing.Size(193, 28);
            this.buttonSaveGroupHealSettings.TabIndex = 6;
            this.buttonSaveGroupHealSettings.Text = "Save Settings";
            this.buttonSaveGroupHealSettings.UseVisualStyleBackColor = true;
            this.buttonSaveGroupHealSettings.Click += new System.EventHandler(this.buttonSaveGroupHealSettings_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DimGray;
            this.panel8.Controls.Add(this.labelFormName);
            this.panel8.Controls.Add(this.pictureBox1);
            this.panel8.Location = new System.Drawing.Point(-1, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(618, 28);
            this.panel8.TabIndex = 2;
            this.panel8.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel8_MouseMove);
            // 
            // labelFormName
            // 
            this.labelFormName.AutoSize = true;
            this.labelFormName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFormName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelFormName.Location = new System.Drawing.Point(5, 5);
            this.labelFormName.Name = "labelFormName";
            this.labelFormName.Size = new System.Drawing.Size(363, 18);
            this.labelFormName.TabIndex = 3;
            this.labelFormName.Text = "zShaman [Enhancement] - Group Heal Settings";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ShamanCCv2Dev.Properties.Resources.zRed;
            this.pictureBox1.Location = new System.Drawing.Point(590, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(2, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(315, 26);
            this.label7.TabIndex = 3;
            this.label7.Text = "This is a DEMO for the Group Heal Settings form.\r\nIt will be updated as I update " +
    "the Group Heal settings.";
            // 
            // checkBoxUseLesserHealingWave
            // 
            this.checkBoxUseLesserHealingWave.AutoSize = true;
            this.checkBoxUseLesserHealingWave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseLesserHealingWave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseLesserHealingWave.Location = new System.Drawing.Point(15, -3);
            this.checkBoxUseLesserHealingWave.Name = "checkBoxUseLesserHealingWave";
            this.checkBoxUseLesserHealingWave.Size = new System.Drawing.Size(185, 20);
            this.checkBoxUseLesserHealingWave.TabIndex = 23;
            this.checkBoxUseLesserHealingWave.Text = "Use Lesser Healing Wave";
            this.checkBoxUseLesserHealingWave.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownLesserHealMana);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.numericUpDownLesserHealHP);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.checkBoxUseLesserHealingWave);
            this.groupBox1.Location = new System.Drawing.Point(7, 213);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 100);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "          ";
            // 
            // numericUpDownLesserHealMana
            // 
            this.numericUpDownLesserHealMana.Location = new System.Drawing.Point(97, 61);
            this.numericUpDownLesserHealMana.Name = "numericUpDownLesserHealMana";
            this.numericUpDownLesserHealMana.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownLesserHealMana.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(7, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 35;
            this.label4.Text = ".... and MP >= ";
            // 
            // numericUpDownLesserHealHP
            // 
            this.numericUpDownLesserHealHP.Location = new System.Drawing.Point(97, 28);
            this.numericUpDownLesserHealHP.Name = "numericUpDownLesserHealHP";
            this.numericUpDownLesserHealHP.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownLesserHealHP.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(12, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 33;
            this.label5.Text = "Use if HP <= ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericUpDownHealingWaveManaMin);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.numericUpDownHealingWaveHPMax);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.checkBoxUseHealingWave);
            this.groupBox2.Location = new System.Drawing.Point(7, 107);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 100);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "          ";
            // 
            // numericUpDownHealingWaveManaMin
            // 
            this.numericUpDownHealingWaveManaMin.Location = new System.Drawing.Point(97, 62);
            this.numericUpDownHealingWaveManaMin.Name = "numericUpDownHealingWaveManaMin";
            this.numericUpDownHealingWaveManaMin.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownHealingWaveManaMin.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(7, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 31;
            this.label3.Text = ".... and MP >= ";
            // 
            // numericUpDownHealingWaveHPMax
            // 
            this.numericUpDownHealingWaveHPMax.Location = new System.Drawing.Point(97, 29);
            this.numericUpDownHealingWaveHPMax.Name = "numericUpDownHealingWaveHPMax";
            this.numericUpDownHealingWaveHPMax.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownHealingWaveHPMax.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(12, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Use if HP <= ";
            // 
            // checkBoxUseHealingWave
            // 
            this.checkBoxUseHealingWave.AutoSize = true;
            this.checkBoxUseHealingWave.Checked = true;
            this.checkBoxUseHealingWave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseHealingWave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseHealingWave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseHealingWave.Location = new System.Drawing.Point(15, -3);
            this.checkBoxUseHealingWave.Name = "checkBoxUseHealingWave";
            this.checkBoxUseHealingWave.Size = new System.Drawing.Size(141, 20);
            this.checkBoxUseHealingWave.TabIndex = 23;
            this.checkBoxUseHealingWave.Text = "Use Healing Wave";
            this.checkBoxUseHealingWave.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDown5);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.numericUpDown6);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.checkBoxUseChainHeal);
            this.groupBox3.Location = new System.Drawing.Point(7, 319);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 135);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "          ";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(97, 59);
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(54, 20);
            this.numericUpDown5.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(7, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 35;
            this.label8.Text = ".... and MP >= ";
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Location = new System.Drawing.Point(97, 26);
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(54, 20);
            this.numericUpDown6.TabIndex = 34;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(12, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 33;
            this.label10.Text = "Use if HP <= ";
            // 
            // checkBoxUseChainHeal
            // 
            this.checkBoxUseChainHeal.AutoSize = true;
            this.checkBoxUseChainHeal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseChainHeal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.checkBoxUseChainHeal.Location = new System.Drawing.Point(15, -3);
            this.checkBoxUseChainHeal.Name = "checkBoxUseChainHeal";
            this.checkBoxUseChainHeal.Size = new System.Drawing.Size(121, 20);
            this.checkBoxUseChainHeal.TabIndex = 23;
            this.checkBoxUseChainHeal.Text = "Use Chain Heal";
            this.checkBoxUseChainHeal.UseVisualStyleBackColor = true;
            // 
            // HealForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(16)))), ((int)(((byte)(8)))));
            this.ClientSize = new System.Drawing.Size(615, 555);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HealForm";
            this.Text = "HealForm";
            this.Load += new System.EventHandler(this.HealForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLesserHealMana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLesserHealHP)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealingWaveManaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHealingWaveHPMax)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboBoxRoleYou;
        private System.Windows.Forms.ComboBox comboBoxRoleP2;
        private System.Windows.Forms.ComboBox comboBoxRoleP5;
        private System.Windows.Forms.ComboBox comboBoxRoleP4;
        private System.Windows.Forms.ComboBox comboBoxRoleP3;
        private System.Windows.Forms.Label labelPlayernameP5;
        private System.Windows.Forms.Label labelPlayernameP4;
        private System.Windows.Forms.Label labelPlayernameP3;
        private System.Windows.Forms.Label labelPlayernameP2;
        private System.Windows.Forms.Label labelPlayernameYou;
        private System.Windows.Forms.Button buttonRefreshGroup;
        private System.Windows.Forms.Label labelHPPercentP5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelHPPercentP4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelHPPercentP3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelHPPercentP2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelHPPercentYou;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button buttonSaveGroupHealSettings;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxUseLesserHealingWave;
        private System.Windows.Forms.CheckBox checkBoxHealP4;
        private System.Windows.Forms.CheckBox checkBoxHealP3;
        private System.Windows.Forms.CheckBox checkBoxHealP2;
        private System.Windows.Forms.CheckBox checkBoxHealP1;
        private System.Windows.Forms.CheckBox checkBoxHealYou;
        private System.Windows.Forms.Label labelFormName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxUseHealingWave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxUseChainHeal;
        private System.Windows.Forms.NumericUpDown numericUpDownHealingWaveManaMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownHealingWaveHPMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownLesserHealMana;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownLesserHealHP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.Label label10;
    }
}