﻿namespace ShamanCCv2Dev.GUI
{
    partial class DevPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBoxClose = new System.Windows.Forms.PictureBox();
            this.richTextBoxAuras = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRefreshAuras = new System.Windows.Forms.Button();
            this.buttonPrintToLog = new System.Windows.Forms.Button();
            this.buttonPrintProgramDirToLog = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonPrintTargetInfo = new System.Windows.Forms.Button();
            this.buttonPrintPlayerInfo = new System.Windows.Forms.Button();
            this.textBoxItemNameToCheck = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBoxClose);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 30);
            this.panel1.TabIndex = 0;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "zShaman Dev Panel";
            // 
            // pictureBoxClose
            // 
            this.pictureBoxClose.Image = global::ShamanCCv2Dev.Properties.Resources.zRed;
            this.pictureBoxClose.Location = new System.Drawing.Point(266, 3);
            this.pictureBoxClose.Name = "pictureBoxClose";
            this.pictureBoxClose.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxClose.TabIndex = 0;
            this.pictureBoxClose.TabStop = false;
            this.pictureBoxClose.Click += new System.EventHandler(this.pictureBoxClose_Click);
            // 
            // richTextBoxAuras
            // 
            this.richTextBoxAuras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.richTextBoxAuras.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxAuras.ForeColor = System.Drawing.Color.White;
            this.richTextBoxAuras.Location = new System.Drawing.Point(15, 79);
            this.richTextBoxAuras.Name = "richTextBoxAuras";
            this.richTextBoxAuras.ReadOnly = true;
            this.richTextBoxAuras.Size = new System.Drawing.Size(259, 188);
            this.richTextBoxAuras.TabIndex = 1;
            this.richTextBoxAuras.Text = "";
            this.richTextBoxAuras.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Auras:";
            // 
            // buttonRefreshAuras
            // 
            this.buttonRefreshAuras.ForeColor = System.Drawing.Color.Black;
            this.buttonRefreshAuras.Location = new System.Drawing.Point(15, 273);
            this.buttonRefreshAuras.Name = "buttonRefreshAuras";
            this.buttonRefreshAuras.Size = new System.Drawing.Size(75, 23);
            this.buttonRefreshAuras.TabIndex = 3;
            this.buttonRefreshAuras.Text = "Refresh";
            this.buttonRefreshAuras.UseVisualStyleBackColor = true;
            this.buttonRefreshAuras.Click += new System.EventHandler(this.buttonRefreshAuras_Click);
            // 
            // buttonPrintToLog
            // 
            this.buttonPrintToLog.ForeColor = System.Drawing.Color.Black;
            this.buttonPrintToLog.Location = new System.Drawing.Point(199, 273);
            this.buttonPrintToLog.Name = "buttonPrintToLog";
            this.buttonPrintToLog.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintToLog.TabIndex = 4;
            this.buttonPrintToLog.Text = "Print To Log";
            this.buttonPrintToLog.UseVisualStyleBackColor = true;
            this.buttonPrintToLog.Click += new System.EventHandler(this.buttonPrintToLog_Click);
            // 
            // buttonPrintProgramDirToLog
            // 
            this.buttonPrintProgramDirToLog.Enabled = false;
            this.buttonPrintProgramDirToLog.ForeColor = System.Drawing.Color.Black;
            this.buttonPrintProgramDirToLog.Location = new System.Drawing.Point(15, 336);
            this.buttonPrintProgramDirToLog.Name = "buttonPrintProgramDirToLog";
            this.buttonPrintProgramDirToLog.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintProgramDirToLog.TabIndex = 5;
            this.buttonPrintProgramDirToLog.Text = "WoW Dir";
            this.buttonPrintProgramDirToLog.UseVisualStyleBackColor = true;
            this.buttonPrintProgramDirToLog.Click += new System.EventHandler(this.buttonPrintProgramDirToLog_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Print To Log:";
            // 
            // buttonPrintTargetInfo
            // 
            this.buttonPrintTargetInfo.ForeColor = System.Drawing.Color.Black;
            this.buttonPrintTargetInfo.Location = new System.Drawing.Point(108, 336);
            this.buttonPrintTargetInfo.Name = "buttonPrintTargetInfo";
            this.buttonPrintTargetInfo.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintTargetInfo.TabIndex = 7;
            this.buttonPrintTargetInfo.Text = "Target Info";
            this.buttonPrintTargetInfo.UseVisualStyleBackColor = true;
            this.buttonPrintTargetInfo.Click += new System.EventHandler(this.buttonPrintTargetInfo_Click);
            // 
            // buttonPrintPlayerInfo
            // 
            this.buttonPrintPlayerInfo.Enabled = false;
            this.buttonPrintPlayerInfo.ForeColor = System.Drawing.Color.Black;
            this.buttonPrintPlayerInfo.Location = new System.Drawing.Point(199, 336);
            this.buttonPrintPlayerInfo.Name = "buttonPrintPlayerInfo";
            this.buttonPrintPlayerInfo.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintPlayerInfo.TabIndex = 8;
            this.buttonPrintPlayerInfo.Text = "Player Info";
            this.buttonPrintPlayerInfo.UseVisualStyleBackColor = true;
            this.buttonPrintPlayerInfo.Click += new System.EventHandler(this.buttonPrintPlayerInfo_Click);
            // 
            // textBoxItemNameToCheck
            // 
            this.textBoxItemNameToCheck.Location = new System.Drawing.Point(15, 378);
            this.textBoxItemNameToCheck.Name = "textBoxItemNameToCheck";
            this.textBoxItemNameToCheck.Size = new System.Drawing.Size(153, 20);
            this.textBoxItemNameToCheck.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(174, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "<- Name To ID";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(148, 405);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(134, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Item Ammount Method B";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(8, 405);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Item Ammount Method A";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DevPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(292, 531);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxItemNameToCheck);
            this.Controls.Add(this.buttonPrintPlayerInfo);
            this.Controls.Add(this.buttonPrintTargetInfo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonPrintProgramDirToLog);
            this.Controls.Add(this.buttonPrintToLog);
            this.Controls.Add(this.buttonRefreshAuras);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBoxAuras);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DevPanel";
            this.Text = "DevPanel";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxClose;
        private System.Windows.Forms.RichTextBox richTextBoxAuras;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRefreshAuras;
        private System.Windows.Forms.Button buttonPrintToLog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonPrintProgramDirToLog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonPrintTargetInfo;
        private System.Windows.Forms.Button buttonPrintPlayerInfo;
        private System.Windows.Forms.TextBox textBoxItemNameToCheck;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
    }
}