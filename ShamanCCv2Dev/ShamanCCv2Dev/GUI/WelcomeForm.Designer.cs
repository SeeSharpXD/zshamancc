﻿namespace ShamanCCv2Dev.GUI
{
    partial class WelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelVersionText = new System.Windows.Forms.Label();
            this.buttonConfigureCC = new System.Windows.Forms.Button();
            this.buttonCloseWelcomeWindow = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabelSeeSharp = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabelForumThread = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ShamanCCv2Dev.Properties.Resources.zShamanLogo;
            this.pictureBox1.Location = new System.Drawing.Point(434, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(327, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to zShaman!";
            // 
            // labelVersionText
            // 
            this.labelVersionText.AutoSize = true;
            this.labelVersionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVersionText.Location = new System.Drawing.Point(12, 42);
            this.labelVersionText.Name = "labelVersionText";
            this.labelVersionText.Size = new System.Drawing.Size(194, 33);
            this.labelVersionText.TabIndex = 2;
            this.labelVersionText.Text = "Version: 0.4.0";
            // 
            // buttonConfigureCC
            // 
            this.buttonConfigureCC.ForeColor = System.Drawing.Color.Black;
            this.buttonConfigureCC.Location = new System.Drawing.Point(443, 210);
            this.buttonConfigureCC.Name = "buttonConfigureCC";
            this.buttonConfigureCC.Size = new System.Drawing.Size(116, 35);
            this.buttonConfigureCC.TabIndex = 3;
            this.buttonConfigureCC.Text = "Configure CC";
            this.buttonConfigureCC.UseVisualStyleBackColor = true;
            this.buttonConfigureCC.Click += new System.EventHandler(this.buttonConfigureCC_Click);
            // 
            // buttonCloseWelcomeWindow
            // 
            this.buttonCloseWelcomeWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCloseWelcomeWindow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonCloseWelcomeWindow.Location = new System.Drawing.Point(15, 210);
            this.buttonCloseWelcomeWindow.Name = "buttonCloseWelcomeWindow";
            this.buttonCloseWelcomeWindow.Size = new System.Drawing.Size(422, 35);
            this.buttonCloseWelcomeWindow.TabIndex = 4;
            this.buttonCloseWelcomeWindow.Text = "Close Welcome Message and start Botting!";
            this.buttonCloseWelcomeWindow.UseVisualStyleBackColor = true;
            this.buttonCloseWelcomeWindow.Click += new System.EventHandler(this.buttonCloseWelcomeWindow_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(14, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(421, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "This window will only be shown on your first use of zShaman.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 29);
            this.label4.TabIndex = 6;
            this.label4.Text = "Coded By:";
            // 
            // linkLabelSeeSharp
            // 
            this.linkLabelSeeSharp.AutoSize = true;
            this.linkLabelSeeSharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelSeeSharp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabelSeeSharp.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabelSeeSharp.Location = new System.Drawing.Point(135, 104);
            this.linkLabelSeeSharp.Name = "linkLabelSeeSharp";
            this.linkLabelSeeSharp.Size = new System.Drawing.Size(89, 20);
            this.linkLabelSeeSharp.TabIndex = 7;
            this.linkLabelSeeSharp.TabStop = true;
            this.linkLabelSeeSharp.Text = "SeeSharp";
            this.linkLabelSeeSharp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSeeSharp_LinkClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(14, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(303, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Please reort any bugs you find to the";
            // 
            // linkLabelForumThread
            // 
            this.linkLabelForumThread.AutoSize = true;
            this.linkLabelForumThread.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelForumThread.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.linkLabelForumThread.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.linkLabelForumThread.Location = new System.Drawing.Point(314, 159);
            this.linkLabelForumThread.Name = "linkLabelForumThread";
            this.linkLabelForumThread.Size = new System.Drawing.Size(121, 20);
            this.linkLabelForumThread.TabIndex = 9;
            this.linkLabelForumThread.TabStop = true;
            this.linkLabelForumThread.Text = "Forum Thread";
            this.linkLabelForumThread.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelForumThread_LinkClicked);
            // 
            // WelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.ClientSize = new System.Drawing.Size(580, 257);
            this.Controls.Add(this.linkLabelForumThread);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.linkLabelSeeSharp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonCloseWelcomeWindow);
            this.Controls.Add(this.buttonConfigureCC);
            this.Controls.Add(this.labelVersionText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WelcomeForm";
            this.Text = "WelcomeForm";
            this.Load += new System.EventHandler(this.WelcomeForm_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.WelcomeForm_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonConfigureCC;
        private System.Windows.Forms.Button buttonCloseWelcomeWindow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabelSeeSharp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabelForumThread;
        public System.Windows.Forms.Label labelVersionText;
    }
}