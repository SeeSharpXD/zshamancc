﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShamanCCv2Dev.Constants;
using ShamanCCv2Dev.MoreMethods;

namespace ShamanCCv2Dev.Settings
{
    public class zShamanSettingsGroupHeal
    {
        public volatile bool healPYou = true;
        public volatile bool healP2 = true;
        public volatile bool healP3 = true;
        public volatile bool healP4 = true;
        public volatile bool healP5 = true;

        public volatile ShamanEnums.GroupTypes groupRolePYou = ShamanEnums.GroupTypes.Undefined;
        public volatile ShamanEnums.GroupTypes groupRoleP2 = ShamanEnums.GroupTypes.Undefined;
        public volatile ShamanEnums.GroupTypes groupRoleP3 = ShamanEnums.GroupTypes.Undefined;
        public volatile ShamanEnums.GroupTypes groupRoleP4 = ShamanEnums.GroupTypes.Undefined;
        public volatile ShamanEnums.GroupTypes groupRoleP5 = ShamanEnums.GroupTypes.Undefined;

        public volatile bool groupHealHealingWave = true;
        public volatile bool groupHealLesserHealingWave = false;
        public volatile bool groupHealChainHeal = false;

        public volatile int groupHealHealingWaveHPPercent = 80;
        public volatile int ggroupHealHealingWaveManaMin = 35;
        public volatile bool groupHealHealingWaveDownskill = true;

        public void SetDefaults()
        {
            try
            {
                healPYou = true;
                healP2 = true;
                healP3 = true;
                healP5 = true;


                groupRolePYou = ShamanEnums.GroupTypes.Undefined;
                groupRoleP2 = ShamanEnums.GroupTypes.Undefined;
                groupRoleP3 = ShamanEnums.GroupTypes.Undefined;
                groupRoleP4 = ShamanEnums.GroupTypes.Undefined;
                groupRoleP5 = ShamanEnums.GroupTypes.Undefined;

                groupHealHealingWave = true;
                groupHealLesserHealingWave = false;
                groupHealChainHeal = false;

                groupHealHealingWaveHPPercent = 80;
                ggroupHealHealingWaveManaMin = 35;
                groupHealHealingWaveDownskill = true;
    }
            catch
            {
                Helpers.PrintToChat("Error Setting Defaults for Group Heal!");
            }
        }
    }
}
