﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using ShamanCCv2Dev.GUI;
using ShamanCCv2Dev.Constants;

namespace ShamanCCv2Dev.Settings
{
    public class CCSettings
    {
        //compatability settings
        public volatile string versionSavedWith = "0.4.0";

        //totem settings
        public volatile ShamanEnums.EarthTotems totemDefaultEarth = ShamanEnums.EarthTotems.Stoneskin;
        public volatile ShamanEnums.FireTotems totemDefaultFire = ShamanEnums.FireTotems.Searing;
        public volatile ShamanEnums.WaterTotems totemDefaultWater = ShamanEnums.WaterTotems.HealingStream;
        public volatile ShamanEnums.AirTotems totemDefaultAir = ShamanEnums.AirTotems.Grounding;

        public volatile bool totemEnabledEarth = true;
        public volatile bool totemEnabledFire = false;
        public volatile bool totemEnabledWater = false;
        public volatile bool totemEnabledAir = false;

        public volatile float maxTotemDistance = 19;

        public volatile bool smartTotem = true;

        //buff settings
        public volatile bool weaponUseBuff = true;
        public volatile ShamanEnums.WeaponBuffs weaponBuffDefault = ShamanEnums.WeaponBuffs.Rockbiter;
        public volatile bool useWaterWalking = false;
        public volatile bool useLightningShield = true;

        //resting settings
        public volatile string restFood = "SET ME";
        public volatile string restDrink = "SET ME";

        public volatile bool restHeal = false;
        public volatile float restHealHPPercent = 30;

        //pull settings
        public volatile float combatPullRange = 19;
        public volatile string combatPullSpell = "Earth Shock";
        public volatile bool combatNoPullOnTargetSkull = true;
        public volatile float combatNoPullOnTargetLevelGreaterBy = 5;
        public volatile float combatNoPullOnTargetLevelLessBy = 59;

        //combat settings
        public volatile ShamanEnums.ShockTypes combatShockTypeDefault = ShamanEnums.ShockTypes.Earth;
        public volatile bool combatUseShockSpells = true;
        public volatile bool combatUseAutoAttack = true;
        public volatile bool combatUseHeals = true;
        public volatile bool combatUseLesserHeal = false;
        public volatile float combatHealBelowPercentHP = 40;
        public volatile float combatLesserHealBelowPercentHP = 75;
        public volatile bool combatUseFlameshockDot = false;
        public volatile bool combatReApplyLightningShield = true;
        public volatile bool combatUseStormstrike = true;
        public volatile float combatKeepAboveManaPercent = 18;

        //group heal settings
        public volatile bool useGroupHeal = true;
        public volatile bool groupHealOnly = false;

        //humanizer settings
        public volatile bool humanizeMakeErrors = true;


        public void SetDefaultConfigValues(ShamanEnums.SettingsPresets settingToLoad)
        {
            switch (settingToLoad)
            {
                case ShamanEnums.SettingsPresets.Default:
                    {
                        versionSavedWith = "0.4.0";

                        //totem settings
                        totemDefaultEarth = ShamanEnums.EarthTotems.Stoneskin;
                        totemDefaultFire = ShamanEnums.FireTotems.Searing;
                        totemDefaultWater = ShamanEnums.WaterTotems.HealingStream;
                        totemDefaultAir = ShamanEnums.AirTotems.Grounding;

                        totemEnabledEarth = true;
                        totemEnabledFire = false;
                        totemEnabledWater = false;
                        totemEnabledAir = false;

                        maxTotemDistance = 19;

                        smartTotem = true;

                        //buff settings
                        weaponUseBuff = true;
                        weaponBuffDefault = ShamanEnums.WeaponBuffs.Rockbiter;
                        useWaterWalking = false;
                        useLightningShield = true;

                        //resting settings
                        restFood = "SET ME";
                        restDrink = "SET ME";

                        restHeal = false;
                        restHealHPPercent = 30;

                        //pull settings
                        combatPullRange = 19;
                        combatPullSpell = "Earth Shock";
                        combatNoPullOnTargetSkull = true;
                        combatNoPullOnTargetLevelGreaterBy = 5;
                        combatNoPullOnTargetLevelLessBy = 59;

                        //combat settings
                        combatShockTypeDefault = ShamanEnums.ShockTypes.Earth;
                        combatUseShockSpells = true;
                        combatUseAutoAttack = true;
                        combatUseHeals = true;
                        combatUseLesserHeal = false;
                        combatHealBelowPercentHP = 40;
                        combatLesserHealBelowPercentHP = 75;
                        combatUseFlameshockDot = false;
                        combatReApplyLightningShield = true;
                        combatUseStormstrike = true;
                        combatKeepAboveManaPercent = 18;

                        MoreMethods.Helpers.PrintToChat("Loaded Settings: Default");
                        break;
                    }
                case ShamanEnums.SettingsPresets.Lowbie:
                    {
                        //totem defaults
                        totemDefaultEarth = ShamanEnums.EarthTotems.Earthbind;
                        totemDefaultFire = ShamanEnums.FireTotems.FlameTongue;
                        totemDefaultWater = ShamanEnums.WaterTotems.HealingStream;
                        totemDefaultAir = ShamanEnums.AirTotems.Grounding;

                        totemEnabledEarth = false;
                        totemEnabledFire = false;
                        totemEnabledWater = false;
                        totemEnabledAir = false;

                        maxTotemDistance = 19;

                        smartTotem = true;

                        //buff defaults
                        weaponUseBuff = true;
                        weaponBuffDefault = ShamanEnums.WeaponBuffs.Rockbiter;
                        useWaterWalking = false;
                        useLightningShield = false;

                        //rest defaults
                        restFood = "Tough Jerky";
                        restDrink = "Refreshing Spring Water";

                        restHeal = true;
                        restHealHPPercent = 35;

                        //Pull defaults
                        combatPullRange = 18;
                        combatPullSpell = "Lightning Bolt";

                        combatNoPullOnTargetSkull = true;
                        combatNoPullOnTargetLevelGreaterBy = 5;
                        combatNoPullOnTargetLevelLessBy = 59;

                        //combat defaults;
                        combatShockTypeDefault = ShamanEnums.ShockTypes.Earth;
                        combatUseShockSpells = false;
                        combatUseAutoAttack = true;
                        combatUseHeals = true;
                        combatUseLesserHeal = false;
                        combatHealBelowPercentHP = 35;
                        combatLesserHealBelowPercentHP = 55;
                        combatUseFlameshockDot = false;
                        combatReApplyLightningShield = false;
                        combatUseStormstrike = false;
                        combatKeepAboveManaPercent = 28;

                        MoreMethods.Helpers.PrintToChat("Loaded Settings: Lowbie");
                        break;
                    }
                case ShamanEnums.SettingsPresets.Experimental:
                    {

                        MoreMethods.Helpers.PrintToChat("Loaded Settings: Experimental");
                        break;
                    }
                case ShamanEnums.SettingsPresets.Raider:
                    {

                        MoreMethods.Helpers.PrintToChat("Loaded Settings: Raider");
                        break;
                    }
                case ShamanEnums.SettingsPresets.PVP:
                    {

                        MoreMethods.Helpers.PrintToChat("Loaded Settings: PVP");
                        break;
                    }
            }
        }
    }
}
